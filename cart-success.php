<?php include('header.php'); ?>
<div data-aos="fade-in">
  <nav class="breadcrumbwrap">
      <div class="container">
          <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="index.php">首頁</a></li>
              <li class="breadcrumb-item"><a href="">購物車</a></li>
              <li class="breadcrumb-item active"><a href="">訂單完成</a></li>
          </ol>
      </div>
  </nav>
	<h1 class="title-page">訂單完成</h1>

  <div class="container page-cart">
    <!-- 進度條 -->
    <div class="w-100">
      <div class="stepper-container mb-5 mt-3">
        <div class="stepper">
          <div class="icon-wrap"><i class="fas fa-shopping-cart"></i></div>
          <div class="label-wrap">
            <div class="title">Step 1</div>
            <div class="desc">訂單明細</div>
          </div>
        </div>

        <div class="stepper-arrow">
          <i class="fas fa-arrow-right"></i>
        </div>

        <div class="stepper">
          <div class="icon-wrap"><i class="fas fa-money-check"></i></div>
          <div class="label-wrap">
            <div class="title">Step 2</div>
            <div class="desc">訂單明細確認 & <br>付款、填寫資料</div>
          </div>
        </div>

        <div class="stepper-arrow">
          <i class="fas fa-arrow-right"></i>
        </div>

        <div class="stepper active">
          <div class="icon-wrap"><i class="fas fa-user-check"></i></div>
          <div class="label-wrap">
            <div class="title">Step 3</div>
            <div class="desc">訂單完成</div>
          </div>
        </div>
      </div>
    </div>

    <div class="row mb-5">
      <!-- TODO:數量 input & 進度條 -->
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-lg-6 col-lg-offset-3">
        <div class="card" style="padding: 35px;border-radius: 5px;border: solid 1px rgba(159, 136, 106, 0.2);">
          <h5 class="p-subject text-center mb-4">您的訂單已成功送出</h5>

          <p class="text-center mb-5">
            您的訂單已經成功送出，我們會在第一時間進行處理！
            <br>
            如您有任何問題或意見，請聯絡我們，
            <br>
            感謝您的訂購！
          </p>

          <div class="clearfix text-center mb-3">
            <div class="btn-box-1">
              <a href="index.php" title="回首頁" class="button-style brown2">回首頁</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php include('footer.php'); ?>

<script>
var carrier = $('[name="carrierType"]');
    carrier.change(TypeChange);
//   	carrierNum.attr("disabled", true);

   	carrier.trigger('change');

function TypeChange(){
  var type = $(carrier).filter(":checked").val();
	var carrierNum = $('.carrierNum');

  carrierNum.slideUp();
  $(carrierNum).filter("[name*=" + type + "Carrier]").slideDown();
}
</script>