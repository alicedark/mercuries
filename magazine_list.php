<?php include('header.php'); ?>
	<nav class="breadcrumbwrap">
		<div class="container">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="index.php">首頁</a></li>
				<li class="breadcrumb-item active"><a href="#">繁盛店情報誌</a></li>
			</ol>
		</div>
	</nav>
	<h1 class="title-page">繁盛店情報誌</h1>
	<div id="article">
		<div class="article-list">
			<div class="right-area">
				<div data-plugins="right-slick">
					<div class="item">
						<a href="book.php" title=""><span class="img"><img src="assets/images/section-1-1-min.png" alt=""></span></a>
						<div class="main">
						</div>
					</div>
					<div class="item">
						<a href="book.php" title=""><span class="img"><img src="assets/images/section-1-2-min.png" alt=""></span></a>
					</div>
					<div class="item">
						<a href="book.php" title=""><span class="img"><img src="assets/images/section-1-3-min.png" alt=""></span></a>
					</div>
					<div class="item">
						<a href="book.php" title=""><span class="img"><img src="assets/images/section-1-1-min.png" alt=""></span></a>
					</div>
					<div class="item">
						<a href="book.php" title=""><span class="img"><img src="assets/images/section-1-1-min.png" alt=""></span></a>
					</div>
					<div class="item">
						<a href="book.php" title=""><span class="img"><img src="assets/images/section-1-2-min.png" alt=""></span></a>
					</div>
				</div>
			</div>
			<div class="left-area">
				<div data-plugins="left-slick">
					<div class="item">
						<div class="title">Vol.4｜桃紅柳綠 就是一見傾心</div>
						<div class="summary">
							誰說浪漫春天一定要被框在甜美路線呢！<br>
							櫻花香氣瀰漫之際，加點屬於自己的風格調性，就是令人陶醉不己。
							誰說浪漫春天一定要被框在甜美路線呢！<br>
							櫻花香氣瀰漫之際，加點屬於自己的風格調性，就是令人陶醉不己。
							誰說浪漫春天一定要被框在甜美路線呢！<br>
							櫻花香氣瀰漫之際，加點屬於自己的風格調性，就是令人陶醉不己。
						</div>
					</div>
					<div class="item">
						<div class="title">Vol.4｜桃紅柳綠 就是一見傾心2</div>
						<div class="summary">
							誰說浪漫春天一定要被框在甜美路線呢！<br>
							櫻花香氣瀰漫之際，加點屬於自己的風格調性，就是令人陶醉不己。
						</div>
					</div>
					<div class="item">
						<div class="title">Vol.4｜桃紅柳綠 就是一見傾心3</div>
						<div class="summary">
							誰說浪漫春天一定要被框在甜美路線呢！<br>
							櫻花香氣瀰漫之際，加點屬於自己的風格調性，就是令人陶醉不己。
						</div>
					</div>
					<div class="item">
						<div class="title">Vol.4｜桃紅柳綠 就是一見傾心4</div>
						<div class="summary">
							誰說浪漫春天一定要被框在甜美路線呢！<br>
							櫻花香氣瀰漫之際，加點屬於自己的風格調性，就是令人陶醉不己。
						</div>
					</div>
					<div class="item">
						<div class="title">Vol.4｜桃紅柳綠 就是一見傾心5</div>
						<div class="summary">
							誰說浪漫春天一定要被框在甜美路線呢！<br>
							櫻花香氣瀰漫之際，加點屬於自己的風格調性，就是令人陶醉不己。
						</div>
					</div>
					<div class="item">
						<div class="title">Vol.4｜桃紅柳綠 就是一見傾心6</div>
						<div class="summary">
							誰說浪漫春天一定要被框在甜美路線呢！<br>
							櫻花香氣瀰漫之際，加點屬於自己的風格調性，就是令人陶醉不己。
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php include('footer.php'); ?>
<script>
$(function() {

	var leftSlick = $('[data-plugins="left-slick"]');
	 leftSlick.slick({
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  arrows: false,
	  fade: true,
	  swipe: false,
  	adaptiveHeight: true,
	  asNavFor: rightSlick
	});

	var rightSlick = $('[data-plugins="right-slick"]');
	rightSlick.slick({
	  slidesToShow: 3,
	  slidesToScroll: 1,
	  arrows: true,
	  dots: false,
	  centerMode: true,
	  variableWidth: true,
	  focusOnSelect: true,
	  cssEase: 'linear',
  	asNavFor: leftSlick,
	  responsive: [
	    {
	      breakpoint: 768,
	      settings: {
				  slidesToShow: 1,
				  slidesToScroll: 1,
	  			centerMode: false,
	  			variableWidth: false
	      }
	    }
	  ]
	});

	var imgs = rightSlick.find('img');
	imgs.each(function(){
	  var item = $(this).closest('.item .img');
	  item.css({
	    'background-image': 'url(' + $(this).attr('src') + ')', 
	    'background-position': 'center',            
	    '-webkit-background-size': 'cover',
	    'background-size': 'cover', 
	  });
	  $(this).hide();
	});
});
</script>