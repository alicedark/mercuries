<?php include('header.php'); ?>
	<nav class="breadcrumbwrap">
		<div class="p-1200">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.php">首頁</a></li>
                <li class="breadcrumb-item active"><a href="#">購物商城</a></li>
            </ol>
		</div>
	</nav>
	<div id="product-list2">
		<div class="p-1200">
			<div class="banner">
				<ul class="reset" data-plugins="pbanner-slick">
					<li><img src="assets/images/shopping-index-bn-min.png" alt=""></li>
					<li><img src="assets/images/shopping-index-bn-min.png" alt=""></li>
					<li><img src="assets/images/shopping-index-bn-min.png" alt=""></li>
				</ul>
			</div>
			<div class="p-content">
				<aside id="aside">
					<a href="category.php" class="button-style brown2 mb-3 w-100">活動新訊</a>
					<?php include('aside.php'); ?>
				</aside>
				<div class="p-main">
					<div class="top-ad">
						<div data-plugins="ad-slick">
							<div class="item">
								<a href="product_view.php" title="">
									<span class="img"><img src="assets/images/shopping-index-ad1-min.png" alt=""></span>
									<span class="title">即期良品促銷- 富澤商店 烘焙材料烘焙材料</span>
									<span class="line"><span></span></span>
								</a>
							</div>
							<div class="item">
								<a href="product_view.php" title="">
									<span class="img"><img src="assets/images/shopping-index-ad2-min.png" alt=""></span>
									<span class="title">即期良品促銷- 富澤商店 烘焙材料烘焙材料</span>
									<span class="line"><span></span></span>
								</a>
							</div>
						</div>
					</div>
					<div class="p-subject">熱門商品</div>
					<div class="product-list2">
						<ul class="reset" data-plugins="product-list2">
							<li>
								<div class="item">
									<a href="product_view.php" title="">
										<span class="img"><img src="assets/images/p1-min.png" alt=""></span>
										<span class="main">
											<span class="title">丸金 特級淡口醬油 1000ml</span>
											<span class="line"><span></span></span>
											<span class="price no-old"><span class="new">NT$ 180</span></span>
										</span>
									</a>
								</div>
							</li>
							<li>
								<div class="item">
									<a href="product_view.php" title="">
										<span class="img"><img src="assets/images/p2-min.png" alt=""></span>
										<span class="main">
											<span class="title">即期良品促銷- 富澤商店 烘焙材料烘焙材料</span>
											<span class="line"><span></span></span>
											<span class="price"><span class="old">NT$ 180</span><span class="new">NT$ 180</span></span>
										</span>
									</a>
								</div>
							</li>
							<li>
								<div class="item">
									<a href="product_view.php" title="">
										<span class="img"><img src="assets/images/p3-min.png" alt=""></span>
										<span class="main">
											<span class="title">丸金 特級淡口醬油 1000ml</span>
											<span class="line"><span></span></span>
											<span class="price no-old"><span class="new">NT$ 180</span></span>
										</span>
									</a>
								</div>
							</li>
							<li>
								<div class="item">
									<a href="product_view.php" title="">
										<span class="img"><img src="assets/images/p1-min.png" alt=""></span>
										<span class="main">
											<span class="title">丸金 特級淡口醬油 1000ml</span>
											<span class="line"><span></span></span>
											<span class="price no-old"><span class="new">NT$ 180</span></span>
										</span>
									</a>
								</div>
							</li>
							<li>
								<div class="item">
									<a href="product_view.php" title="">
										<span class="img"><img src="assets/images/p2-min.png" alt=""></span>
										<span class="main">
											<span class="title">即期良品促銷- 富澤商店 烘焙材料烘焙材料</span>
											<span class="line"><span></span></span>
											<span class="price"><span class="old">NT$ 180</span><span class="new">NT$ 180</span></span>
										</span>
									</a>
								</div>
							</li>
							<li>
								<div class="item">
									<a href="product_view.php" title="">
										<span class="img"><img src="assets/images/p3-min.png" alt=""></span>
										<span class="main">
											<span class="title">丸金 特級淡口醬油 1000ml</span>
											<span class="line"><span></span></span>
											<span class="price no-old"><span class="new">NT$ 180</span></span>
										</span>
									</a>
								</div>
							</li>
						</ul>
					</div>
					<div class="banner-img"><img src="assets/images/shopping-index-bn2-min.png" alt=""></div>
					<div class="p-subject">推薦商品</div>
					<div class="product-list2">
						<ul class="reset" data-plugins="product-list2">
							<li>
								<div class="item">
									<a href="product_view.php" title="">
										<span class="img"><img src="assets/images/p1-min.png" alt=""></span>
										<span class="main">
											<span class="title">丸金 特級淡口醬油 1000ml</span>
											<span class="line"><span></span></span>
											<span class="price"><span class="new">NT$ 180</span></span>
										</span>
									</a>
								</div>
							</li>
							<li>
								<div class="item">
									<a href="product_view.php" title="">
										<span class="img"><img src="assets/images/p2-min.png" alt=""></span>
										<span class="main">
											<span class="title">即期良品促銷- 富澤商店 烘焙材料烘焙材料</span>
											<span class="line"><span></span></span>
											<span class="price"><span class="old">NT$ 180</span><span class="new">NT$ 180</span></span>
										</span>
									</a>
								</div>
							</li>
							<li>
								<div class="item">
									<a href="product_view.php" title="">
										<span class="img"><img src="assets/images/p3-min.png" alt=""></span>
										<span class="main">
											<span class="title">丸金 特級淡口醬油 1000ml</span>
											<span class="line"><span></span></span>
											<span class="price"><span class="new">NT$ 180</span></span>
										</span>
									</a>
								</div>
							</li>
							<li>
								<div class="item">
									<a href="product_view.php" title="">
										<span class="img"><img src="assets/images/p1-min.png" alt=""></span>
										<span class="main">
											<span class="title">丸金 特級淡口醬油 1000ml</span>
											<span class="line"><span></span></span>
											<span class="price"><span class="new">NT$ 180</span></span>
										</span>
									</a>
								</div>
							</li>
							<li>
								<div class="item">
									<a href="product_view.php" title="">
										<span class="img"><img src="assets/images/p2-min.png" alt=""></span>
										<span class="main">
											<span class="title">即期良品促銷- 富澤商店 烘焙材料烘焙材料</span>
											<span class="line"><span></span></span>
											<span class="price"><span class="old">NT$ 180</span><span class="new">NT$ 180</span></span>
										</span>
									</a>
								</div>
							</li>
							<li>
								<div class="item">
									<a href="product_view.php" title="">
										<span class="img"><img src="assets/images/p3-min.png" alt=""></span>
										<span class="main">
											<span class="title">丸金 特級淡口醬油 1000ml</span>
											<span class="line"><span></span></span>
											<span class="price"><span class="new">NT$ 180</span></span>
										</span>
									</a>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

	
	<?php include('footer.php'); ?>

<script src="assets/js/wan-spinner.js"></script>
<script>
$(function() {
	$('[data-plugins="pbanner-slick"]').slick({
	  infinite: true,
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  autoplay: true,
	  arrows: true,
	  dots: true
	});
	$('[data-plugins="product-list2"]').slick({
	  infinite: true,
	  slidesToShow: 3,
	  slidesToScroll: 1,
	  autoplay: true,
	  arrows: true,
	  dots: false,
	  responsive: [
	    {
	      breakpoint: 1281,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 3
	      }
	    },
	    {
	      breakpoint: 768,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2
	      }
	    }
	  ]
	});
	$('[data-plugins="ad-slick"]').slick({
		mobileFirst: true,
	  infinite: true,
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  autoplay: true,
	  arrows: true,
	  dots: false,
	  responsive: [
	    {
	      breakpoint: 767,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2
	      }
	    }
	  ]
	});

  $('#aside .menu-list li.has_menu>a').data('switch', 'open').on('click', function(e) {
    e.preventDefault();
    var $ele = $(this), $eleLi = $ele.parent('li');
    if ($ele.data('switch') == 'open') {
      $ele.data('switch', 'close');
      $eleLi.addClass('open');
    } else {
      $ele.data('switch', 'open');
      $eleLi.removeClass('open');
    }
  });

  $('#aside .menu-list .subject').data('switch', 'open').on('click', function(e) {
    var $eleThis = $('#aside .menu-list');
    if ($eleThis.data('switch') == 'open') {
      $eleThis.data('switch', 'close').addClass('open');
    } else {
      $eleThis.data('switch', 'open').removeClass('open');
    }
  });
});
</script>