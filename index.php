<?php include('header.php'); ?>
	<div id="i-banner" data-aos="fade-in">
		<ul class="reset" data-plugin="banner-slick">
			<li>
				<div class="item">
					<div class="background-img">
						<img src="assets/images/bn-min.png" alt="" class="desktop">
						<img src="assets/images/bn-m.png" alt="" class="mobile">
					</div>
					<div class="cover">
						<div class="title">三商食品，<br>優質日系商品代理</div>
						<div class="summary">
							國內許多知名商品均為三商食品代理經銷，其中包括玉極閣燒酎、黑霧島本格燒酎、黃櫻清酒、松竹梅清酒、小山本家清酒…等日系酒類；<br>
							以及日本KEWPIE、BULLDOG、金印山葵、日本天日鹽、<br>日之出味醂、丸金醬油…等調味料品。
						</div>
						<div class="btn-box">
							<a href="about.php" title="更多三商" class="button-style">更多三商</a>
						</div>
					</div>
				</div>
			</li>
			<li>
				<div class="item">
					<div class="background-img">
						<img src="assets/images/bn-min.png" alt="" class="desktop">
						<img src="assets/images/bn-m.png" alt="" class="mobile">
					</div>
					<div class="cover">
						<div class="title">Bnnaer主標題</div>
						<div class="summary">
							文字描述區域，最多不超過四行，超過截斷。文字描述區域，最多不超過四行，超過截斷。文字描述區域，最多不超過四行，超過截斷。文字描述區域，最多不超過四行，超過截斷。文字描述區域，最多不超過四行，超過截斷。文字描述區域，最多不超過四行，超過截斷。文字描述區域，最多不超過四行，超過截斷。文字描述區域，最多不超過四行，超過截斷。文字描述區域，最多不超過四行，超過截斷。文字描述區域，最多不超過四行，超過截斷。文字描述區域，最多不超過四行，超過截斷。文字描述區域，最多不超過四行，超過截斷。文字描述區域，最多不超過四行，超過截斷。文字描述區域，最多不超過四行，超過截斷。文字描述區域，最多不超過四行，超過截斷。文字描述區域，最多不超過四行，超過截斷。文字描述區域，最多不超過四行，超過截斷。文字描述區域，最多不超過四行，超過截斷。文字描述區域，最多不超過四行，超過截斷。文字描述區域，最多不超過四行，超過截斷。文字描述區域，最多不超過四行，超過截斷。文字描述區域，最多不超過四行，超過截斷。文字描述區域，最多不超過四行，超過截斷。文字描述區域，最多不超過四行，超過截斷。文字描述區域，最多不超過四行，超過截斷。文字描述區域，最多不超過四行，超過截斷。文字描述區域，最多不超過四行，超過截斷。文字描述區域，最多不超過四行，超過截斷。文字描述區域，最多不超過四行，超過截斷。文字描述區域，最多不超過四行，超過截斷。文字描述區域，最多不超過四行，超過截斷。
						</div>
						<div class="btn-box">
							<a href="about.php" title="更多三商" class="button-style">更多三商</a>
						</div>
					</div>
				</div>
			</li>
		</ul>
	</div>
	<div id="i-product">
		<div class="p-1200">
			<div class="p-subject">
				<div class="tw">三商<span>首選</span></div>
			</div>
			<div class="summary">啤酒為世界上最古老的飲料之一，數年間啤酒的釀造工藝不斷進步，拓展至世界的各個角落。</div>
			<div class="product-list">
				<!-- 01 -->
				<div class="item">
					<div class="img">
						<a href="product_list5_b.php" title="">
							<img src="assets/images/section-1-1-min.png" alt="" class="desktop">
							<img src="assets/images/section-1-1-m.png" alt="" class="mobile">
						</a>
					</div>
					<div class="main">
						<div class="title"><span>桶啤</span></div>
						<div class="summary">
							啤酒為世界上最古老的飲料之一，數年間啤酒的釀造工藝不斷進步，拓展至世界的各個角落
						</div>
						<div class="btn-box">
							<a href="product_list5_b.php" title="詳細介紹" class="button-style">詳細介紹</a>
						</div>
					</div>
				</div>
				<!-- /01 -->
				<div class="item">
					<div class="img">
						<a href="product_list5_b.php" title="">
							<img src="assets/images/section-1-2-min.png" alt="" class="desktop">
							<img src="assets/images/section-1-2-m.png" alt="" class="mobile">
						</a>
					</div>
					<div class="main">
						<div class="title"><span>日本葡萄酒</span></div>
						<div class="summary">
							日本葡萄酒是能夠令人謙虛、引人入勝，更是能讓人重拾搭餐樂趣的怡人美麗
						</div>
						<div class="btn-box">
							<a href="product_list5_b.php" title="詳細介紹" class="button-style">詳細介紹</a>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="img">
						<a href="product_list.php" title="">
							<img src="assets/images/section-1-3-min.png" alt="" class="desktop">
							<img src="assets/images/section-1-3-m.png" alt="" class="mobile">
						</a>
					</div>
					<div class="main">
						<div class="title"><span>購物商城</span></div>
						<div class="summary">
							溫暖而細膩的日本南部風情，感受日本前十大清酒品牌之極致釀造工藝。對於釀造過程中微生物的研究更是專精各類燒酎第一品牌齊聚冠軍榮耀一次品嚐
						</div>
						<div class="btn-box">
							<a href="product_list.php" title="進入商城" class="button-style">進入商城</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="i-inquiry" data-aos="fade-up">
		<div class="p-1200">
			<div class="main">
				<div class="p-subject">
					<div class="tw">營業用<span>詢價</span></div>
					<div class="summary">
						我們除了扮演商品銷售的角色，<br>更提供專業的銷售諮詢和建議，<br>是餐廳、零售店家、連鎖通路不可或缺的經營顧問夥伴。</div>
				</div>
				<div class="btn-box">
					<a href="contact.php" title="聯絡我們" class="button-style">聯絡我們</a>
				</div>
			</div>
		</div>
	</div>
	<div id="i-section">
		<div class="p-1200">
			<div class="section-list">
				<!-- 01 -->
				<a href="magazine_list.php" title="" class="item" data-aos="fade-up">
					<span class="img"><img src="assets/images/section-2-1-min.png" alt=""></span>
					<span class="title">繁盛店情報誌</span>
				</a>
				<!-- /01 -->
				<a href="" title="" class="item" data-aos="fade-up">
					<span class="img"><img src="assets/images/section-2-2-min.png" alt=""></span>
					<span class="title">珈琲鑑定士</span>
				</a>

				<!-- 此為特殊區塊:外連到獨立頁面 (sour3-single-page) -->
				<a href="sour3-single-page/index.php"  target="_blank" title="SOUR3" class="item" data-aos="fade-up">
					<span class="img"><img src="assets/images/section-2-3-min.png" alt=""></span>
					<span class="title">SOUR3</span>
				</a>
				<a href="" title="" class="item" data-aos="fade-up">
					<span class="img"><img src="assets/images/section-2-4-min.png" alt=""></span>
					<span class="title">品嚐據點</span>
				</a>
			</div>
		</div>
	</div>

<?php include('footer.php'); ?>
<script>
$(function() {
	var banSlick = $('[data-plugin="banner-slick"]');
	banSlick.on('init', function(event, slick) {
      banSlick.find('.slick-slide[data-slick-index="0"] .item').addClass('view');
    }).on('beforeChange', function(event, slick, currentSlide, nextSlide) {
      $('.slick-slide:not([data-slick-index="' + nextSlide + '"]) .item', banSlick).removeClass('view');
      $('.slick-slide[data-slick-index="' + nextSlide + '"] .item', banSlick).addClass('view');
    }).slick({
    fade: true,
		dots: false,
		arrows: true
	});
});
</script>