<?php include('header.php'); ?>
<div data-aos="fade-in" class="page-shopping">
	<nav class="breadcrumbwrap">
		<div class="p-1200">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.php">首頁</a></li>
                <li class="breadcrumb-item"><a href="">購物商城</a></li>
                <li class="breadcrumb-item"><a href="">第一層</a></li>
                <li class="breadcrumb-item active"><a href="#">酒類</a></li>
            </ol>
		</div>
	</nav>
	
	<div id="product-list">
        <div class="p-1200">
            <div class="s-bn mb-4" style="background-image:url(assets/images/shopping-index-bn-min.png)">
                <div class="text-box">
                    <!-- <h1>購物商城</h1> -->
                </div>
            </div>
			<div class="p-content">
				<aside id="aside">
				<?php include('aside.php'); ?>
				</aside>
                <div class="p-main">
                    <div class="product-card-list">
                        <a href="product_list4_a.php" class="product-card">
                            <div class="bg" style="background-image:url(assets/images/section-1-1-min.png)"></div>
                            <div class="text-box">
                                <h1>大分類A</h1>
                            </div>
                        </a>
                        <a href="product_list4_a.php" class="product-card">
                            <div class="bg" style="background-image:url(assets/images/section-1-1-min.png)"></div>
                            <div class="text-box">
                                <h1>大分類B</h1>
                            </div>
                        </a>
                        <a href="product_list4_a.php" class="product-card">
                            <div class="bg" style="background-image:url(assets/images/section-1-1-min.png)"></div>
                            <div class="text-box">
                                <h1>大分類C</h1>
                            </div>
                        </a>
                        <a href="product_list4_a.php" class="product-card">
                            <div class="bg" style="background-image:url(assets/images/section-1-1-min.png)"></div>
                            <div class="text-box">
                                <h1>大分類D</h1>
                            </div>
                        </a>
                        <a href="product_list4_a.php" class="product-card">
                            <div class="bg" style="background-image:url(assets/images/section-1-1-min.png)"></div>
                            <div class="text-box">
                                <h1>大分類E</h1>
                            </div>
                        </a>
                        <a href="product_list4_a.php" class="product-card">
                            <div class="bg" style="background-image:url(assets/images/section-1-1-min.png)"></div>
                            <div class="text-box">
                                <h1>大分類F</h1>
                            </div>
                        </a>
                    </div>
                </div>
			</div>
        </div>
	</div>
</div>

<div class="modal" id="age-warn" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body text-center">
		<h3 class="subtitle-page mt-3 mb-2">您滿18歲了嗎?</h3>
		<p>*您必須超過18歲，才能看到酒類商品內容</p>
      </div>
      <div class="modal-footer">
		<div class="text-center">
			<button type="button" class="btn btn-main" data-dismiss="modal">是，我已年滿18歲</button>
			<button type="button" class="btn btn-secondary">否</button>
		</div>
      </div>
    </div>
  </div>
</div>
<?php include('footer.php'); ?>

<script>
    $('#aside .menu-list li.has_menu>a').data('switch', 'open').on('click', function(e) {
    e.preventDefault();
    var $ele = $(this), $eleLi = $ele.parent('li');
    if ($ele.data('switch') == 'open') {
      $ele.data('switch', 'close');
      $eleLi.addClass('open');
    } else {
      $ele.data('switch', 'open');
      $eleLi.removeClass('open');
    }
  });
</script>