
	<?php include('header.php'); ?>
	<div data-aos="fade-in" class="page-info">
        <nav class="breadcrumbwrap">
            <div class="container">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.php">首頁</a></li>
                    <li class="breadcrumb-item active"><a href="">購物須知</a></li>
                </ol>
            </div>
        </nav>
        <h1 class="title-page">購物須知</h1>
        <div class="container" style="margin-bottom: 40px;">
            <div class="col-md-3 col-md-push-9 pl-sm-5">

                <!-- <select class="form-control for-mb mt-3">
                    <option>選擇子類別</option>
                    <option><a href="notes.php#note-1">購物流程</a></option>
                    <option>個人詢問單流程</option>
                    <option>會員相關</option>
                    <option>統一發票</option>
                    <option>配送相關</option>
                    <option>取消訂單/個人詢問單</option>
                    <option>退換貨說明</option>
                    <option>營業用詢價</option>
                    <option>商品常見問題</option>
                    <option>防詐騙宣導</option>
                </select> -->
                <div class="menu-list" style="margin-top:70px;">
                    <ul class="reset">
                        <li>
                            <a href="notes.php#note-1" title="購物流程">購物流程</a>
                        </li>
                        <li>
                            <a href="notes.php#note-2" title="個人詢問單流程">個人詢問單流程</a>
                        </li>
                        <li>
                            <a href="notes.php#note-3" title="會員相關">會員相關</a>
                        </li>
                        <li>
                            <a href="notes.php#note-4" title="統一發票">統一發票</a>
                        </li>
                        <li>
                            <a href="notes.php#note-5" title="配送相關">配送相關</a>
                        </li>
                        <li>
                            <a href="notes.php#note-6" title="取消訂單/個人詢問單">取消訂單/個人詢問單</a>
                        </li>
                        <li>
                            <a href="notes.php#note-7" title="退換貨說明">退換貨說明</a>
                        </li>
                        <li>
                            <a href="notes.php#note-8" title="營業用詢價">營業用詢價</a>
                        </li>
                        <li>
                            <a href="notes.php#note-9" title="商品常見問題">商品常見問題</a>
                        </li>
                        <li>
                            <a href="notes.php#note-10" title="防詐騙宣導">防詐騙宣導</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-9 col-md-pull-3">
                <div class="sec-block anchor-point-target anchor-point-target" id="note-1">
                    <h1 class="subtitle-page">購物流程</h1>
                    <img src="assets/images/notes_1.jpg" class="rwd-img" alt="">
                    <ul>
                        <li>請註冊/登入會員，即可立即線上購物。</li>
                        <li>本網站線上購物付款方式為「線上刷卡」。</li>
                        <li>訂單處理狀態可至「訂單查詢」確認。</li>
                        <li>全館免運，訂單總金額達門檻即可成立訂單。(訂單滿額出貨門檻：一般宅配(常溫商品)訂單1,000元、冷凍宅配訂單1,000元；一般宅配訂單與冷凍訂單成立個別訂單，恕無法共同計算總金額。)</li>
                    </ul>
                </div>
                <div class="sec-block anchor-point-target" id="note-2">
                    <h1 class="subtitle-page">個人詢問單流程</h1>
                    <img src="assets/images/notes_2.jpg" class="rwd-img" alt="">
                    <p>依據菸酒管理法第30條，酒之販賣或轉讓，不得以自動販賣機、郵購、電子購物或其他無法辨識購買者或受讓者年齡等方式為之。</p>
                    <p>本網站不提供酒類商品線上購物，個人詢問單為本網站所提供酒類商品的資詢服務，完成個人詢問單並不代表完成交易，我們將有專人與您聯繫。</p>
                    <p>
                        <ul>
                            <li>請註冊/登入會員，即可使用個人詢問單服務。</li>
                            <li>個人詢問單處理狀態可至「個人詢問單」確認。</li>
                            <li>如經專人確認您未滿18歲，我們將直接取消您的詢問單。</li>
                            <li>個人詢問單總金額1,000元，即可完成詢問單(詢問單並非訂購，恕無法與購物訂單共同計算總金額)。</li>
                        </ul>
                    </p>
                </div>
                <div class="sec-block anchor-point-target" id="note-3">
                    <h1 class="subtitle-page">會員相關</h1>
                    <p>
                        <ul>
                            <li>如您忘記密碼，請於登入位置下方點選「忘記密碼」，我們將傳送一組新密碼至您註冊帳號的e-mail，收到新密碼重新登錄後，您可至會員專區修改密碼。</li>
                            <li>請務必確認會員資料為本人正確、真實且完整的資訊，避免產生後續相關服務處理問題。</li>
                        </ul>
                    </p>
                </div>
                <div class="sec-block anchor-point-target" id="note-4">
                    <h1 class="subtitle-page">統一發票</h1>
                    <p>
                        <ul>
                            <li>本網站所開立統一發票將隨商品一同寄送至收件地址。</li>
                            <li>如您需要開立統編，敬請於確認商品明細時點選三聯發票並填寫統一編號與抬頭。</li>
                        </ul>
                    </p>
                </div>
                <div class="sec-block anchor-point-target" id="note-5">
                    <h1 class="subtitle-page">配送相關</h1>
                    <p>
                        <ul>
                            <li>訂單成立後，約3-5個工作天到貨。</li>
                            <li>本網站暫不提供離島地區配送服務。</li>
                            <li>常溫商品與冷凍商品為個別成立訂單配送，冷凍商品將採低溫冷凍車配送，以確保商品品質。</li>
                            <li>商品配送皆為本網站合作宅配業者代為運送，本網站會在訂單成立後盡快為您安排出貨，然實際配送時間與過程為宅配業者所調度，非本網站可控之責，如遇配送相關問題，請聯絡我們，我們會為您向宅配業者確認運送進度。</li>
                        </ul>
                    </p>
                </div>
                <div class="sec-block anchor-point-target" id="note-6">
                    <h1 class="subtitle-page">取消訂單/個人詢問單</h1>
                    <p>訂單/個人訊問單成立後，如您想取消，您可至「訂單查詢」/「個人詢問單」取消，一旦商品出貨後，恕無法取消。</p>
                </div>
                <div class="sec-block anchor-point-target" id="note-7">
                    <h1 class="subtitle-page">退換貨說明</h1>
                    <p>本網站商品皆為食品，除下述不可退換貨之情形外，則商品於到貨7日內皆可接受退換貨，請務必在收貨當下確認商品狀況，以保障您的權益。</p>
                    <h4 class="mt-3"><b>不可退換貨之情形</b></h4>
                    <p>
                        <ul>
                            <li>已拆封商品：基於食品衛生考量，一經拆封恕無法辦理退貨。</li>
                            <li>人為損害商品：非商品本身瑕疵，而是人為造成損害。</li>
                            <li>正確訂單明細商品：到貨商品與商品明細相符合。</li>
                            <li>主觀喜惡商品：不符合個人主觀喜惡(不想吃了、不喜歡、看起來不好吃)。</li>
                            <li>生鮮食品(冷凍商品)：生鮮食品(冷凍商品)不適用於消費者保護法第19條，並不享有7天鑑賞期，因無法掌握您收到冷凍商品後的保存狀況，恕不辦理退貨。</li>
                            <p><b>上述商品皆不可退換貨。</b></p>
                        </ul>
                    </p>
                    <p>退換貨流程 聯繫客服說明商品狀況>以原紙箱完整包裝其商品>客服安排宅配業者到府取件>客服收到退貨商品後辦理退款或換貨</p>
                    <p>退款：「發票」需與所有商品一同包裝，退款金額為發票金額。</p>
                    <p>換貨：僅需完整包裝欲退換貨商品(換同品項)。</p>
                </div>
                <div class="sec-block anchor-point-target" id="note-8">
                    <h1 class="subtitle-page">營業用詢價</h1>
                    <p>如您為業務需求的營業單位、企業或需要大量採購特定商品，請至聯絡我們留言，提供您的單位名稱(或店家類型)、地址、連絡電話，我們會立即指派業務專人為您服務。</p>
                </div>
                <div class="sec-block anchor-point-target" id="note-9">
                    <h1 class="subtitle-page">商品常見問題</h1>
                    <p>
                        <b>Q 商品都是現貨嗎?</b><br>
                        A 本網站所有商品皆為現貨，但如遇特殊商品銷售狀況，導致商品缺貨則無法加入購物車。
                    </p>
                    <p>
                        <b>Q 進口農產品與國產食品要如何購買?</b><br>
                        A 進口農產品「季節的恩惠」為三商食品嚴選的季節性水果，為特定時間販售的預購產品，如欲購買，敬請注意本網站訊息；而國產食品商品分類中的「油品」、「飲料」、「乾貨與調味品」暫不開放線上購物，造成不便敬請見諒。
                    </p>
                    <p>
                        <b>Q 商品圖與實體會有差異嗎?</b><br>
                        A 每台裝置螢幕或多或少會有顏色差異，商品狀況以實物為準。
                    </p>
                    <p>
                        <b>Q 同時購買常溫、冷凍、酒類商品是否可以合併訂單金額?</b><br>
                        A 常溫商品出貨方式為一般宅配，冷凍商品出貨方式為冷凍宅配，常溫、冷凍商品為獨立訂單配送恕無法合併訂單總金額；線上無法購買酒類商品，恕無法與任何購物訂單合併配送。
                    </p>
                </div>
                <div class="sec-block anchor-point-target" id="note-10">
                    <h1 class="subtitle-page">防詐騙宣導</h1>
                    <p>提醒您，請小心求證並注意不明來電，務必提高警覺，請勿輕易相信以本網站名義通知您處理以下事項：<br>
                    我們不會主動通知網路訂單重複扣款、訂購數量誤植、誤刷，我們沒有提供分期付款服務，所以也不會有分期設定錯誤狀況。<br>
                    我們不會主動要求提供帳戶、信用卡…等任何完整的個人金融資料。<br>
                    我們不會要求您去ATM執行任何操作，如：解除分期、取消交易、餘額查詢…等任何帳戶訊息。<br>
                    我們不會主動要求您提供本網站會員帳號、密碼。<br>
                    </p>
                    <p>如有可疑來電，請與我們確認或撥打165反詐騙諮詢專線查證，以確保安全。</p>
                    <p>三商食品股份有限公司 關心您自身權益<br>
                    服務電話：0800-004-433(服務時間：周一至周五 9:00~18:00)<br>
                    聯絡信箱：service@mlf.com.tw</p>
                </div>
            </div>
        </div>
    </div>

<!-- my footer start -->

<?php include('footer.php'); ?>