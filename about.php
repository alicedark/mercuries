
<?php include('header.php'); ?>
<div data-aos="fade-in" class="page-info bg-gray">
    <nav class="breadcrumbwrap">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.php">首頁</a></li>
                <li class="breadcrumb-item active"><a href="#">企業介紹</a></li>
            </ol>
        </div>
    </nav>
    <h1 class="title-page">企業介紹</h1>
    <div class="container">
        <div class="sec-block">
            <h1 class="subtitle-page">
                基本資料
            </h1>
            <table class="table1">
                <tr>
                    <th>總公司地址</th>
                    <td>新北市五股區五權路57號</td>
                </tr>
                <tr>
                    <th>總公司電話</th>
                    <td>(02)2299-2833</td>
                </tr>
                <tr>
                    <th>設立</th>
                    <td>2008年10月</td>
                </tr>
                <tr>
                    <th>負責人</th>
                    <td>董事長 陳翔玢</td>
                </tr>
                <tr>
                    <th>資本額</th>
                    <td>NT$200,000,000</td>
                </tr>
                <tr>
                    <th>員工</th>
                    <td>130位</td>
                </tr>
            </table>
        </div>
        <div class="sec-block">
            <h1 class="subtitle-page">
                創立背景
            </h1>
            <p>三商食品，源於三商企業1990年成立的食品事業部，主要經營酒類、調味品類的代理及銷售。</p>
            <p>國內許多知名商品均為三商食品代理經銷，其中包括玉極閣燒酎、黑霧島本格燒酎、黃櫻清酒、松竹梅清酒、小山本家清酒…等日系酒類；以及日本KEWPIE、BULLDOG、金印山葵、日本天日鹽、日之出味醂、丸金醬油…等調味料品。近來，更將代理產品線擴充至冷凍食材…等，企圖擴大版圖。</p>
            <p>為了提供更完善的管理績效，三商食品在台北、桃園、新竹、台中、高雄皆設有辦公室和倉儲配送中心，眾多據點，只為締造極佳服務。</p>
        </div>
        <div class="sec-block">
            <h1 class="subtitle-page">
                六大知識
            </h1>
            <h3>單一窗口全省物流配送</h3>
            <p class="sub-text">配送網絡遍布四大區域，一天提供三次配送服務，一周六天效率不變。早上高雄下單，下午台北到貨！更有免付費專線，提供訂貨服務及申訴管道，在通路上深受好評。</p>
            <h3>專業酒品銷售及餐廳經營顧問</h3>
            <p class="sub-text">除了銷售，三商食品也提供經營方面的顧問服務。從酒品知識、市場訊息、銷售建議，甚至協助舉辦清酒祭、日本週…等活動，更提供冰箱、啤酒機…等設備免費租借服務，以及海報、壁貼、布條、菜單、酒單…等各式店頭宣傳物的設計服務。</p>
            <h3>直營銷售網絡</h3>
            <p class="sub-text">為了提升品質，三商食品堅持不透過經銷商，而是以直營體系，直接了解市場反應與客戶心聲，為客戶提供服務。遍布台灣的直營網絡，目前已服務超過90%以上的日本料理店和約莫五千家餐廳。</p>
            <img src="assets/images/about_1.png" alt="" class="rwd-img" style="max-width: 900px;">
            <h3>強大的營業管理</h3>
            <p class="sub-text">三商食品在全省共有五個營業所，分布在台北、桃園、新竹、台中、高雄，均由專業的直營銷售團隊組成，具有敏捷快速的市場反應能力，面面俱到，全面兼顧。<br>
                全套客製化的ERP系統，處理單據更加迅速；同時結合系統車派單，讓物流出車配送更加迅速。穩定而先進的後勤系統，讓前線業務人員無後顧之憂，展現極優的營業管理能力。</p>
            <h3>商品價格優勢</h3>
            <p class="sub-text">擁有量販店、便利超商、連鎖超市、零售店、餐飲店…等近兩萬家客戶，能大量採購降低成本，並透過直營網路直接回饋顧客。</p>
            <h3>隸屬三商行集團</h3>
            <p class="sub-text">在台灣通路市場結構急遽變動下，三商食品結合三商行集團，擁有全台最大規模的直營網絡，因此具有極高的商品開發力及專業的行銷知識，能提供更多元化的服務。</p>
        </div>
    </div>
    <section class="bg-deco">
        <div class="container">
            <div class="row location-info">
                <div class="col-md-10 col-md-offset-2">
                    <div class="single-info">
                        <span>台北 | </span>
                        <a href="https://www.google.com/maps/place/248新北市五股區五權路57號">新北市五股區五權路57號</a>
                        <a href="tel:+886-2-22992833">電話：02-2299-2833</a>
                    </div>
                    <div class="single-info">
                        <span>桃園 | </span>
                        <a href="https://www.google.com/maps/place/桃園市蘆竹區南崁路二段323號">桃園市蘆竹區南崁路二段323號</a>
                        <a href="tel:+886-3-3213911">電話：03-321-3911</a>
                    </div>
                    <div class="single-info">
                        <span>新竹 | </span>
                        <a href="https://www.google.com/maps/place/新竹縣竹北市復興二路229號11F-8">新竹縣竹北市復興二路229號11F-8</a>
                        <a href="tel:+886-3-6676733">電話：03-667-6733</a>
                    </div>
                    <div class="single-info">
                        <span>台中 | </span>
                        <a href="https://www.google.com/maps/place/台中市南屯區新鎮和路528號-8">台中市南屯區新鎮和路528號-8</a>
                        <a href="tel:+886-4-24796603">電話：04-2479-6603</a>
                    </div>
                    <div class="single-info">
                        <span>高雄 | </span>
                        <a href="https://www.google.com/maps/place/台中市南屯區新鎮和路528號-8">台中市南屯區新鎮和路528號-8</a>
                        <a href="tel:+886-7-3745133">電話：07-374-5133</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<!-- my footer start -->

<?php include('footer.php'); ?>