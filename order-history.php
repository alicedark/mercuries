<?php include('header.php'); ?>
<div class="page-account" data-aos="fade-in">
  <nav class="breadcrumbwrap">
      <div class="container">
          <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="index.php">首頁</a></li>
              <li class="breadcrumb-item"><a href="">會員中心</a></li>
              <li class="breadcrumb-item active"><a href="">詢問單紀錄</a></li>
          </ol>
      </div>
  </nav>
  <h1 class="title-page">詢問單紀錄</h1>

  <!-- "訂單紀錄" 和 "詢問單紀錄" 共用此頁面 -->
  <div class="container">

    <div class="row">
      <div class="col-xs-12">
        <table class="table table-bordered table-hover mb-5">
          <thead class="text-second">
            <tr class="active">
              <td>訂單編號</td>
              <td>日期</td>
              <td class="hidden-xs">客戶</td>
              <td class="hidden-xs">商品</td>
              <td>詢問單狀態<!--訂單狀態--></td>
            </tr>
          </thead>
          <tbody>
            <?php for ($i=0; $i< 4; $i++): ?>
            <tr>
              <td><a class="text-muted" href="orderlist.php">#111</a></td>
              <td><a class="text-muted" href="orderlist.php">2020-10-27</a></td>
              <td class="hidden-xs"><a class="text-muted" href="orderlist.php">王曉明</a></td>
              <td class="hidden-xs"><a class="text-muted" href="orderlist.php">共1件 / NT $1,000</a></td>
              <td><a class="text-muted" href="orderlist.php">待付款</a></td>
            </tr>
            <?php endfor; ?>
          </tbody>
        </table>

        <div class="text-center mb-5">
          <div class="btn-box-1">
            <a href="account.php" class="button-style back">返回</a>
            <!-- <a href="#" title="註冊" class="button-style brown2">註冊</a> -->
          </div>
        </div>
      </div>

    </div>
  </div>
</div>

<?php include('footer.php'); ?>
