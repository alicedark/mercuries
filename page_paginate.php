<div class="p-paginate">
<!-- 當前頁數加上.current -->
  <!-- 若為第一頁或最後一頁,prev及next加上.disable -->
  <!-- 頁碼最多放3個 -->
  <a href="#" title="上一頁" class="prev control disable">上一頁</a>
  <div class="num">
	  <a href="#" title="第1頁">1</a>
	  <a href="#" title="第2頁">2</a>
	  <a href="#" title="第3頁" class="current">3</a>
	  <a href="#" title="第4頁">4</a>
	  <a href="#" title="第5頁">5</a>
  </div>
  <a href="#" title="下一頁" class="next control">下一頁</a>
</div>