$(function() {

  var header = $('#header'), nav = $('#nav');
  $(window).on('scroll',function(){
    var bodyTop = $(this).scrollTop();
    if(bodyTop >= 90){
    	header.addClass('is-scroll');
    } else{
    	header.removeClass('is-scroll');
    }
  }).trigger('resize');

  $('#nav-icon').data('switch', 'open').on('click', function(e) {
    var $eleThis = $(this);
    if ($eleThis.data('switch') == 'open') {
      $eleThis.data('switch', 'close').addClass('open');
      nav.addClass('is-open');
    	header.addClass('is-scroll');
    } else {
      $eleThis.data('switch', 'open').removeClass('open');
      nav.removeClass('is-open');
    	header.removeClass('is-scroll');
    }
  });

  $('#nav li.has-child > a').data('switch', 'open').on('click', function(e) {
    var $eleThis = $(this).parents('li');
    if ($eleThis.data('switch') == 'open') {
      $eleThis.data('switch', 'close').addClass('open');
    } else {
      $eleThis.data('switch', 'open').removeClass('open');
    }
  });
});

AOS.init({
  easing: 'ease-in-out-sine',
  duration: 1000,
  offset: 300
});

$(document).ready(function() {  
  $("#age-warn").modal('show');

  $("input[name='isRestaurant']").click(function() {
    var isRestaurantVal = $("input[name='isRestaurant']:checked").val();
    if(isRestaurantVal==0){
      $("#restaurantInput").css("display","block");
    }else{
      $("#restaurantInput").css("display","none");
    }
  });


});