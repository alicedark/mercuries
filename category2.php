
	<?php include('header.php'); ?>
	<div data-aos="fade-in" class="page-category page-shopping">
        <div class="bn">
            <img src="assets/images/product-bn.jpg" alt="" class="rwd-img">
        </div>
        <nav class="breadcrumbwrap">
            <div class="container">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.php">首頁</a></li>
                    <li class="breadcrumb-item active"><a href="">限時活動  熱銷商品滿額免運</a></li>
                </ol>
            </div>
        </nav>
        <h1 class="title-page">限時活動  熱銷商品滿額免運</h1>
        <div class="container">
            <div class="desc-page">
                <p><b>活動時間：即日起~2020/9/15</b></p>
                <p>活動內容：購買 「珈琲鑑定士 經典微糖(30入/箱)」 一箱，即隨貨贈送同商品一箱；購買2箱送2箱，以此類推。</p>
                <p>註：限定促銷活動，非即期商品。</p>
            </div>
        </div>

        <div class="container" style="margin-bottom: 40px;">
            <div class="product-card-list mt-4 grids-4">
                <div class="item">
                    <div class="box">
                        <div class="img"><img src="assets/images/product_3.jpg" alt="*" class="rwd-img"></div>
                        <div class="main">
                            <a href="product_view.php" class="title">SOUR3沙瓦 芭樂柳橙風味 350ml（24入）</a>
                            <div class="price">
                                <div class="new">NT$1,050</div>
                                <div class="old">NT$1,280</div>
                            </div>
                            <a class="more">
                                <button name="" type="" class="button-style brown">加入購物車</button>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="box">
                        <div class="img"><img src="assets/images/product_1.jpg" alt="*" class="rwd-img"></div>
                        <div class="main">
                            <a href="product_view.php" class="title">SOUR3沙瓦 芭樂柳橙風味 350ml（24入）</a>
                            <div class="price">
                                <div class="new">NT$1,050</div>
                                <div class="old">NT$1,280</div>
                            </div>
                            <a href="" class="more">
                                <button name="" type="" class="button-style brown">立即詢問</button>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="box">
                        <div class="img"><img src="assets/images/product_2.jpg" alt="*" class="rwd-img"></div>
                        <div class="main">
                            <a href="product_view.php" class="title">SOUR3沙瓦 芭樂柳橙風味 350ml（24入）</a>
                            <div class="price">
                                <div class="new">NT$1,050</div>
                                <div class="old">NT$1,280</div>
                            </div>
                            <a class="more">
                                <button name="" type="" class="button-style brown">加入購物車</button>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="box">
                        <div class="img"><img src="assets/images/product_3.jpg" alt="*" class="rwd-img"></div>
                        <div class="main">
                            <a href="product_view.php" class="title">SOUR3沙瓦 芭樂柳橙風味 350ml（24入）</a>
                            <div class="price">
                                <div class="new">NT$1,050</div>
                                <div class="old">NT$1,280</div>
                            </div>
                            <a class="more">
                                <button name="" type="" class="button-style brown">加入購物車</button>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="box">
                        <div class="img"><img src="assets/images/product_1.jpg" alt="*" class="rwd-img"></div>
                        <div class="main">
                            <a href="product_view.php" class="title">SOUR3沙瓦 芭樂柳橙風味 350ml（24入）</a>
                            <div class="price">
                                <div class="new">NT$1,050</div>
                                <div class="old">NT$1,280</div>
                            </div>
                            <a class="more">
                                <button name="" type="" class="button-style brown">加入購物車</button>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="box">
                        <div class="img"><img src="assets/images/product_3.jpg" alt="*" class="rwd-img"></div>
                        <div class="main">
                            <a href="product_view.php" class="title">SOUR3沙瓦 芭樂柳橙風味 350ml（24入）</a>
                            <div class="price">
                                <div class="new">NT$1,050</div>
                                <div class="old">NT$1,280</div>
                            </div>
                            <a class="more">
                                <button name="" type="" class="button-style brown">加入購物車</button>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="box">
                        <div class="img"><img src="assets/images/product_1.jpg" alt="*" class="rwd-img"></div>
                        <div class="main">
                            <a href="product_view.php" class="title">SOUR3沙瓦 芭樂柳橙風味 350ml（24入）</a>
                            <div class="price">
                                <div class="new">NT$1,050</div>
                                <div class="old">NT$1,280</div>
                            </div>
                            <a class="more">
                                <button name="" type="" class="button-style brown">加入購物車</button>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="box">
                        <div class="img"><img src="assets/images/product_2.jpg" alt="*" class="rwd-img"></div>
                        <div class="main">
                            <a href="product_view.php" class="title">SOUR3沙瓦 芭樂柳橙風味 350ml（24入）</a>
                            <div class="price">
                                <div class="new">NT$1,050</div>
                                <div class="old">NT$1,280</div>
                            </div>
                            <a href="" class="more">
                                <button name="" type="" class="button-style brown">立即詢問</button>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="item"></div>
                <div class="item"></div>
            </div>

            <hr>
            <div class="ad-section">
                <a class="ad-box">
                    <div class="bg" style="background-image:url(assets/images/shopping-index-ad1-min.png)"></div>
                </a>
                <a class="ad-box">
                    <div class="bg" style="background-image:url(assets/images/shopping-index-ad2-min.png)"></div>
                </a>
            </div>
        </div>
    </div>

<!-- my footer start -->

<?php include('footer.php'); ?>