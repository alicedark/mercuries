<div class="search-box">
    <form action="">
        <input type="text" value="" placeholder="產品搜尋...">
        <button type=""><i class="fas fa-search"></i></button>
    </form>
</div>
<div class="menu-list">
    <div class="subject">酒類</div>
    <ul class="reset">
        <li class="has_menu open">
            <a href="#" title="酒類">酒類</a>
            <ul class="reset">
                <li class="has_menu open">
                    <a href="#" title="大分類A">大分類A</a>
                    <ul class="reset">
                        <li><a href="product_list4_a.php" title="">品牌Ａ品牌Ａ品牌Ａ品牌</a></li>
                        <li><a class="active" href="product_list4_a.php" title="">品牌Ｂ</a></li>
                    </ul>
                </li>
                <li class="has_menu">
                    <a href="#" title="大分類A">大分類B</a>
                    <ul class="reset">
                        <li><a href="product_list4_a.php" title="">品牌Ａ</a></li>
                        <li><a href="product_list4_a.php" title="">品牌Ｂ</a></li>
                    </ul>
                </li>
                <li class="has_menu">
                    <a href="#" title="大分類A">大分類C</a>
                    <ul class="reset">
                        <li><a href="product_list4_a.php" title="">品牌Ａ</a></li>
                        <li><a href="product_list4_a.php" title="">品牌Ｂ</a></li>
                    </ul>
                </li>
            </ul>
        </li>
        <li>
            <a href="#" title="食品調味料">食品調味料</a>
            <ul class="reset">
                <li>
                    <a href="#" title="大分類A">大分類A</a>
                    <ul class="reset">
                        <li><a href="product_list4_a.php" title="">品牌Ａ品牌Ａ品牌Ａ品牌</a></li>
                        <li><a href="product_list4_a.php" title="">品牌Ｂ</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#" title="大分類A">大分類B</a>
                    <ul class="reset">
                        <li><a href="product_list4_a.php" title="">品牌Ａ</a></li>
                        <li><a href="product_list4_a.php" title="">品牌Ｂ</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#" title="大分類A">大分類C</a>
                    <ul class="reset">
                        <li><a href="product_list4_a.php" title="">品牌Ａ</a></li>
                        <li><a href="product_list4_a.php" title="">品牌Ｂ</a></li>
                    </ul>
                </li>
            </ul>
        </li>
        <li>
            <a href="#" title="富澤商店">富澤商店</a>
            <ul class="reset">
                <li>
                    <a href="#" title="大分類A">大分類A</a>
                    <ul class="reset">
                        <li><a href="product_list4_a.php" title="">品牌Ａ品牌Ａ品牌Ａ品牌</a></li>
                        <li><a href="product_list4_a.php" title="">品牌Ｂ</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#" title="大分類A">大分類B</a>
                    <ul class="reset">
                        <li><a href="product_list4_a.php" title="">品牌Ａ</a></li>
                        <li><a href="product_list4_a.php" title="">品牌Ｂ</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#" title="大分類A">大分類C</a>
                    <ul class="reset">
                        <li><a href="product_list4_a.php" title="">品牌Ａ</a></li>
                        <li><a href="product_list4_a.php" title="">品牌Ｂ</a></li>
                    </ul>
                </li>
            </ul>
        </li>
    </ul>
</div>