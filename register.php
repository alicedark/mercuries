<?php include('header.php'); ?>
<div class="page-account" data-aos="fade-in">
  <nav class="breadcrumbwrap">
      <div class="container">
          <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="index.php">首頁</a></li>
              <li class="breadcrumb-item active"><a href="">加入會員</a></li>
          </ol>
      </div>
  </nav>
  <h1 class="title-page">加入會員</h1>


  <div class="container px-5 pb-lg-5 pb-4">

    <div class="row">
      <div class="col-12 col-lg-10 col-lg-offset-1">
        <p class="text-muted mb-5">
            親愛的客戶，基於個人資料安全，我們需請您提供以下資料，以確認您的會員身份，方可啟動您在網站的權益，謝謝您的合作！
            <br>
            以下欄位請確實填寫完整，不然會造成後續訂貨及出貨的錯誤。謝謝！（*為必填欄位）
          </p>

        <div>
          <form action="success.php">
            <div class="row">
              <div class="col-lg-12">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group mb-4">
                      <label>姓名 <span class="text-danger">*</span></label>
                      <input class="form-control" type="text" />
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group mb-4">
                      <label>會員帳號 <span class="text-danger">*</span></label>
                      <input class="form-control" type="email" placeholder="請輸入 email" />
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group mb-4">
                      <label>密碼 <span class="text-danger">*</span></label>
                      <input class="form-control" type="password" placeholder="英文或數字6-20碼" />
                    </div>
                  </div>

                  <div class="col-sm-6">
                    <div class="form-group mb-4">
                      <label>確認密碼 <span class="text-danger">*</span></label>
                      <input class="form-control" type="password" />
                    </div>
                  </div>

                  <div class="col-sm-6">
                    <div class="form-group mb-4">
                      <label>性別 <span class="text-danger">*</span></label>
                      <br>
                      <div class="radio-inline">
                        <label>
                          <input type="radio" name="gender" id="optionsRadios1" value="m" checked>
                          男
                        </label>
                      </div>

                      <div class="radio-inline">
                        <label>
                          <input type="radio" name="gender" id="optionsRadios2" value="f">
                          女
                        </label>
                      </div>

                      <div class="text-danger w-100">請選擇性別</div>
                    </div>
                  </div>

                  <div class="col-sm-6">
                    <div class="form-group mb-4">
                      <label>生日 <span class="text-danger">*</span></label>
                      <input class="form-control py-0" type="date" />
                      <div class="text-danger w-100">請選擇生日</div>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group mb-4">
                      <label>行動電話 <span class="text-danger">*</span></label>
                      <input class="form-control" type="text" placeholder="請輸入台灣手機號碼. 範例:0911000123" />
                    </div>
                  </div>

                  <div class="col-sm-6">
                    <div class="form-group mb-4">
                      <label>室內電話</label>
                      <input class="form-control" type="text" />
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-lg-12">
                <?php include('_address_combo.php'); ?>
                <div class="row">

                  <div class="col-sm-12">
                    <div class="form-group mb-4">
                      <label>是否為餐飲店家? <span class="text-danger">*</span></label>
                      <br>
                      <div class="radio-inline">
                        <label>
                          <input id="is_business" type="radio" name="is_business" value="y" checked>
                          是
                        </label>
                      </div>

                      <div class="radio-inline">
                        <label>
                          <input type="radio" name="is_business" value="n">
                          否
                        </label>
                      </div>

                      <input class="form-control" name="" id="business_txt" placeholder="敬請填寫店名，若為籌備中店家則略過!"></input>
                    </div>
                  </div>

                  <div class="col-sm-12 mb-4">
                    <div class="checkbox">
                      <label>
                        <input type="checkbox"> 訂閱電子報，以便接收最新優惠及活動訊息！
                        <!-- 後台會員資料中顯示該會員是否有訂閱電子報, 並且該欄位包含在匯出的會員資料中, 不需要額外製作電子報的後台 -->
                      </label>
                    </div>
                  </div>

                </div>
              </div>
            </div>
            <div class="row mb-5">
              <div class="col-sm-6">
                  <label>驗證碼<span class="text-danger">*</span></label>

                  <div class="clearfix">
                    <input type="text" name="captcha" id="input-captcha" class="form-control pull-left" style="width: calc(100% - 163px - 1rem);">
                    <img src="assets/images/capcha.jpg" alt="captcha" class="pull-right" style="height: calc(1.5em + 0.75rem - 2px);">
                  </div>

                  <span class="text-danger">驗證碼輸入錯誤</span>

              </div>
            </div>
            <div class="text-center">
              <div class="btn-box-1">
                <!-- <button class="button-style brown2">註冊</button> -->
                <a href="login.php" title="確認送出" class="button-style brown2">確認送出</a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<?php include('footer.php'); ?>

<script>
  $(function () {
    var inp_name = $('#is_business').attr('name')
    var radio_business = $(`input[name='${inp_name}']`)
    var inp_is_business = $('#is_business')
    // init
    if (inp_is_business.is(':checked')) toggleTxtArea()
    // change event
    radio_business.on('change', toggleTxtArea);

    function toggleTxtArea () {
      console.log('toggle')

      if (inp_is_business.is(':checked')) {
        console.log('inp-is business')
        $('#business_txt').show(500)
      } else {
        $('#business_txt').hide(500)
        $('#business_txt').val('')
      }
    }

  })
</script>