<?php include('header.php'); ?>
<div data-aos="fade-in" class="page-shopping">
    <div class="bn">
        <img src="assets/images/product-bn.jpg" alt="" class="rwd-img">
    </div>
    <nav class="breadcrumbwrap">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.php">首頁</a></li>
                <li class="breadcrumb-item active"><a href="#">餐飲用產品</a></li>
            </ol>
        </div>
    </nav>
    <h1 class="title-page">餐飲用產品</h1>
	<div id="product-list">
        <div class="p-1200">
            <div class="product-card-list mb-2">
                <a href="product_list3_b.php" class="product-card for-4">
                    <div class="bg" style="background-image:url(assets/images/section-1-1-min.png)"></div>
                    <div class="text-box">
                        <h1>酒類</h1>
                    </div>
                </a>
                <a href="product_list3_b.php" class="product-card for-4">
                    <div class="bg" style="background-image:url(assets/images/section-1-1-min.png)"></div>
                    <div class="text-box">
                        <h1>食品調味料</h1>
                    </div>
                </a>
                <a href="product_list3_b.php" class="product-card for-4">
                    <div class="bg" style="background-image:url(assets/images/section-1-1-min.png)"></div>
                    <div class="text-box">
                        <h1>國內產品</h1>
                    </div>
                </a>
                <a href="product_list3_b.php" class="product-card for-4">
                    <div class="bg" style="background-image:url(assets/images/section-1-1-min.png)"></div>
                    <div class="text-box">
                        <h1>產品分類名稱</h1>
                    </div>
                </a>
                <a href="product_list3_b.php" class="product-card for-4">
                    <div class="bg" style="background-image:url(assets/images/section-1-1-min.png)"></div>
                    <div class="text-box">
                        <h1>產品分類名稱</h1>
                    </div>
                </a>
                <a href="product_list3_b.php" class="product-card for-4">
                    <div class="bg" style="background-image:url(assets/images/section-1-1-min.png)"></div>
                    <div class="text-box">
                        <h1>產品分類名稱</h1>
                    </div>
                </a>
                <a class="for-4"></a>
                <a class="for-4"></a>
            </div>
        </div>
	</div>
</div>
<?php include('footer.php'); ?>