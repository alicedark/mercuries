$(function(){

    var $bubble=$('.bubble'),
        $bubble2=$('.bubble2'),
        $bubble3=$('.bubble3'),
        rate=10,
        s=0,
        speed1=.5,
        speed2=.8,
        speed3=1.2

    function bubbleAnimate(){
        if(s<10000){
            s++;
        }else{
            s=0;
        }

        $bubble.css({
            'background-position-y':Math.floor(-s)*speed1+'px'
        })
        $bubble2.css({
            'background-position-y':Math.floor(-s)*speed2+'px'
        })
        $bubble3.css({
            'background-position-y':Math.floor(-s)*speed3+'px'
        })
        // console.log(s)
    }

    setInterval(bubbleAnimate,rate)


});
