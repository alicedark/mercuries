<!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- <meta name="viewport" content="width=1200" /> -->
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="description" content="日本原裝進口的Sour3果汁沙瓦，高果汁、低酒精，讓你「一點點醉做自己」。 各大通路甜蜜發售中！" />
<meta property="og:title" content="Sour3"></meta>
<meta property="og:description" content="日本原裝進口的Sour3果汁沙瓦，高果汁、低酒精，讓你「一點點醉做自己」。 各大通路甜蜜發售中！"></meta>
<meta property="og:site_name" content="Sour3"></meta>
<title>Sour3</title>
<link href="css/normalize.css" rel="stylesheet" type="text/css" />
<link href="css/index.css" rel="stylesheet" type="text/css" />
<link href="css/media_query.css" rel="stylesheet" type="text/css" />
<link href="icomoon/style.css" rel="stylesheet" type="text/css" />
<link href="css/fullpage.css" rel="stylesheet"  />
<link href="css/fullpage-forcase.css" rel="stylesheet"  />
<!-- bookmark -->
<link rel="Bookmark" href="favicon.ico" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<!--  -->
<!-- <script type="text/javascript" src="js/jquery-1.11.0.min.js"></script> -->
 <!--  -->
<!-- <link href="icomoon/style.css" rel="stylesheet" type="text/css" /> -->
<!-- jQuery.js v1.11.1 -->
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> -->

  <!-- This following line is optional. Only necessary if you use the option css3:false and you want to use other easing effects rather than "linear", "swing" or "easeInOutCubic". -->
  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.9.5/vendors/jquery.easings.min.jss"></script> -->

  <!-- This following line is only necessary in the case of using the option `scrollOverflow:true` -->








<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="js/jquery.preload.js"></script>
<script>
	$(function(){

		$.preload(
			'images/kv-bg.jpg',
			'images/kv-doll.png',
			'images/kv-face1.png',
			'images/kv-face2.png',
			'images/kv-note.png',
			'images/kv-slogan.png',
			'images/kv-pdt1.png',
			'images/kv-pdt2.png',
			'images/kv-tag.png',
			'images/kv-title.png'
		);
	})
</script>

<style type="text/css">
	#fp-nav{
		display: none;
	}
</style>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-111815378-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-111815378-1');
</script>

</head>
<body>
	<div class="cover"><span><img src="images/landscape.gif"></span></div>
	<div class="wrap">
		<header>
			<a href="#"><img src="images/logo.png"></a>
		</header>
		<nav>
			<!-- <div class="switch"><span class="icon icon-icon06"></span></div> -->
		</nav>


		<div class="main" id="fullpage">
			<!-- block1 -->
			<div class="section">
				<div class="s0">
					<div class="content">
						<div class="s0-1"><img src="images/s0-1.jpg"></div>
						<div class="right">
							<div class="s0-2"><img src="images/s0-2.jpg"></div>
							<ul class="choice">
								<li><a href="main.php">YES</a></li>
								<li><a href="#" onClick="alert('您尚未年滿十八歲!!')">NO</a></li>
							</ul>

						</div>
					</div>

				</div>
			</div>
		</div>

		<footer>
			<div>禁止酒駕 <img src="images/tag01.png"> 飲酒過量 有害健康</div>
			<!-- <div class="mobile"><img src="images/waring.gif"></div> -->
		</footer>
	</div>

<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script> -->
<!-- scrollreveal JS File -->
<script src="js/scrollreveal.min.js"></script>
<script src="js/scrollreveal_control.js"></script>
<!--  -->
<script src="fullpage/fullpage.js"></script>


<!-- fullpage plugin -->
<script>
    $(document).ready(function(){
    	var s=1;
        $('#fullpage').fullpage({
            sectionsColor: ['#ffe1e0'],
	        responsiveWidth: 100
        });

    });

    var setWidth = $(window).width();

    $(window).resize(function() {
    	if(setWidth != $(window).width())
    	{
    		window.location.reload();
    	}
	});
</script>


</body>
</html>