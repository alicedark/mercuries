<!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- <meta name="viewport" content="width=1200" /> -->
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="description" content="日本原裝進口的Sour3果汁沙瓦，高果汁、低酒精，讓你「一點點醉做自己」。 各大通路甜蜜發售中！" />
<meta property="og:title" content="Sour3"></meta>
<meta property="og:description" content="日本原裝進口的Sour3果汁沙瓦，高果汁、低酒精，讓你「一點點醉做自己」。 各大通路甜蜜發售中！"></meta>
<meta property="og:site_name" content="Sour3"></meta>
<title>Sour3</title>
<link href="css/normalize.css" rel="stylesheet" type="text/css" />
<link href="css/index.css" rel="stylesheet" type="text/css" />
<link href="css/media_query.css" rel="stylesheet" type="text/css" />
<link href="icomoon/style.css" rel="stylesheet" type="text/css" />
<!-- <link href="icomoon-2/style.css" rel="stylesheet" type="text/css" /> -->
<link href="css/animate.css" rel="stylesheet" type="text/css" />
<link href="css/fullpage.css" rel="stylesheet"  />
<link href="css/fullpage-forcase.css" rel="stylesheet"  />
<!-- bookmark -->
<link rel="Bookmark" href="favicon.ico" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<!--  -->
 <!--  -->
<!-- <link href="icomoon/style.css" rel="stylesheet" type="text/css" /> -->
<!-- jQuery.js v1.11.1 -->
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> -->

  <!-- This following line is optional. Only necessary if you use the option css3:false and you want to use other easing effects rather than "linear", "swing" or "easeInOutCubic". -->
  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.9.5/vendors/jquery.easings.min.jss"></script> -->

  <!-- This following line is only necessary in the case of using the option `scrollOverflow:true` -->
<!-- <script type="text/javascript" src="js/jquery-1.11.0.min.js"></script> -->
<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="js/jquery.preload.js"></script>
<script>
	$(function(){

		$.preload(
			'images/kv-bg.jpg',
			'images/kv-doll.png',
			'images/kv-face1.png',
			'images/kv-face2.png',
			'images/kv-note.png',
			'images/kv-slogan.png',
			'images/kv-pdt1.png',
			'images/kv-pdt2.png',
			'images/kv-tag1.png',
			'images/kv-tag2.png',
      		'images/kv-title.png'
		);
	})
</script>


<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-111815378-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-111815378-1');
</script>
</head>
<body>
	<div class="cover"><span><img src="images/landscape.gif"></span></div>
	<div class="wrap">
		<header>
			<a href="#"><img src="images/logo.png"></a>
		</header>
		<nav>
			<div class="switch"><span class="icon icon-icon06"></span></div>
			<div class="menu-mobile">
				<span class="icon icon-icon07"></span>
				<span class="icon icon-icon08"></span>
				<ul id="menu">
					<li data-menuanchor="firstPage"><a href="#firstPage"><span class="icon icon-icon01"></span><span class="nav-txt">首頁 <span>Home</span></span></a></li>
					<li data-menuanchor="secondPage"><a href="#secondPage"><span class="icon icon-icon02"></span><span class="nav-txt">品牌介紹 <span>About Sour3</span></span></a></li>
					<li data-menuanchor="3rdPage"><a href="#3rdPage"><span class="icon icon-icon03"></span><span class="nav-txt">口味介紹 <span>Flavor</span></span></a></li>
					<li data-menuanchor="4rdPage"><a href="#4rdPage"><span class="icon icon-icon04"></span><span class="nav-txt">銷售通路 <span>Channel</span></span></a></li>
					<li><a href="https://www.instagram.com/sour3_tw/?hl=zh-tw" target="_blank"><span class="icon icon-icon05"></span><span class="nav-txt">IG粉絲團 <span>Instagram</span></span></a></li>
				</ul>
			</div>
		</nav>
		<div class="dark"><div class="cover"></div></div>
		<div class="bar"></div>
		<div class="other">
			<!-- click下一場景 -->
			<div id="actions">
			    <a id="instagram" href="https://www.instagram.com/sour3_tw/?hl=zh-tw" target="_blank"></a>
			    <!-- <a id="moveSectionDown" href="#"></a> -->
			<!-- click回到top -->
				<div id="menu">
					<span data-menuanchor="firstPage" class="active"><a href="#firstPage" class="menu_gotop" id="menu_gotop"></a></span>
					<span class="active"><a id="moveSectionDown" href="#"></a></span>
				</div>
			</div>
		</div>

		<div class="main" id="fullpage">
			<!-- block1 -->
			<div class="section">
				<div class="s1">
          <!-- 假連結,連到飲料頁 -->
          <div class="kv-title"><img src="images/kv-title.png"></div>
          <!--  -->
					<div class="kv-pdt animated delay-2s bounceInUp">
						<div class="kv-note"><img src="images/kv-note.png"></div>
						<div class="kv-pdt1">
							<div class="ani">
								<img src="images/kv-pdt1.png">
							</div>
						</div>
						<div class="kv-pdt2">
							<div class="ani2">
								<img src="images/kv-pdt3.png">
							</div>
						</div>
						<div class="kv-pdt3">
							<div class="ani3">
								<img src="images/kv-pdt2.png">
							</div>
						</div>
					</div>
					<div class="kv-doll">
						<div class="kv-slogan"><img src="images/kv-slogan.png"></div>
						<div class="kv-tag">
              <a href="#3rdPage">
                <img src="images/kv-tag2.png" class="tag2">
                <img src="images/kv-tag1.png" class="tag1">
              </a>
            </div>
						<div class="kv-face2"><img src="images/kv-face2.png"></div>
						<div class="kv-face1"><img src="images/kv-face1.png"></div>
						<div class="doll"><img src="images/kv-doll.png"></div>
					</div>
					<div class="bgmask">xx</div>
				</div>
			</div>
			<!-- block2 -->
			<div class="section">
				<div class="s2 bubble">
					<div class="bubble2">
						<div class="bubble3">
							<div class="block4 what"><a href="#"><img src="images/what.png"></a></div>
							<div class="what-lightbox">
								<div class=""><a href="#"><span class="icon-icon09"></span></a></div>
								<div class="what-content mobile"><img src="images/what-content-mobile.png"></div>
								<div class="what-content pc"><img src="images/what-content.png"></div>
								<!-- <div class="txts">
									<span class="icon-icon10"></span>
									<span class="txt">氣泡感調酒、低酒精濃度、口味酸甜</span>
								</div> -->
							</div>
							<div class="content">
								<div class="block1"><img src="images/scene2-1.png"></div>
								<div class="block2"><img src="images/scene2-2.png"></div>
								<div class="block3">
									一點點醉做自己<br>
									臉紅的樣子，釋放了你平常壓抑的可愛<br>
									神經都放鬆了，展現了藏在你心底的有趣<br>
									迷濛的雙眼，讓本來就迷人的你更會放電<br>
									一點點醉的時候<br>
									你不再壓抑，更自在做自己<br>
									手上拿著Sour3沙瓦的時尚包裝，<br class="br">襯托著你一點點醉的樣子<br>
									反而比平常更美更有魅力
								</div>
								<!-- <div class="block3"><img src="images/scene2-3.png"></div> -->
							</div>
						</div>
					</div>
				</div>
				<div class="bubble-cover"></div>
			</div>
			<!-- block3 -->
			<div class="section">
				<div class="bubble-cover2"></div>
				<div class="s3 bubble">
					<div class="bubble2">
						<div class="bubble3">
							<div class="content">
								<div class="pdt">
					 				<div class="pdt1 pp">
										<div class="pdt"><img src="images/s3-2.png"></div>
									</div>
									<div class="pdt2 pp">
										<div class="pdt"><img src="images/s3-4.png"></div>
									</div>
									<div class="pdt3 pp">
										<div class="pdt"><img src="images/s3-8.png"></div>
									</div>
									<div class="pdt4 pp">
										<div class="pdt"><img src="images/s3-12.png"></div>
									</div>
									<div class="pdt5 pp">
										<div class="pdt"><img src="images/s3-16.png"></div>
									</div>
								</div>
								<ul class="btn">
									<li class="apple"><a href="#" class="selected"><span class="icon-btn1"></span></a></li>
									<li class="peach"><a href="#"><span class="icon-btn2"></span></a></li>
									<li class="grape"><a href="#"><span class="icon-btn3"></span></a></li>
									<li class="bale"><a href="#"></a></li>
									<li class="straw"><a href="#"></a></li>
								</ul>
								<div class="txts">
					 				<div class="txt1 pp">
										<div class="title"><img src="images/s3-1.png"></div>
										<div class="txt">
											蘋果濃郁的氣息與清爽的甜，<br>
											每一口都沁涼爽脆。<br>
											25%的蘋果汁，給你25%紅撲撲蘋果光，<br>
											<span style="color:#de0a44">＂一點點醉可愛！＂</span>
										</div>
									</div>
									<div class="txt2 pp">
										<div class="title"><img src="images/s3-3.png"></div>
										<div class="txt">
											甜香四溢的水蜜桃，喝一口就停不了。<br>
											25%的白桃汁，給你25%桃氣電力，<br>
											<span style="color:#fc949e"><span style="color:#ffeab8">＂</span>一點點醉迷人！<span style="color:#ffeab8">＂</span></span>
											<!-- 撐開高度 -->
											<div style="color:#fff;">.</div>
										</div>
									</div>
                  <div class="txt3 pp">
										<div class="title"><img src="images/s3-9.png"></div>
										<div class="txt">
											清新香甜的白葡萄搭配細緻的氣泡，<br>
											爽口的後韻交疊出迷人的層次！<br>
											30%的白葡萄汁，給你30%做白日夢的勇氣，<br>
											<span style="color:#aac27f">＂一點點醉勇敢！＂</span>
										</div>
									</div>
                  <div class="txt4 pp">
										<div class="title"><img src="images/s3-13.png"></div>
										<div class="txt">
											芭樂溫潤的甜結合柳丁的清爽酸味，<br>
                      台灣人都懂的絕妙「辦桌味」！<br>
                      高含量的芭樂柳橙汁，給你「樂」作劇的淘氣，<br>
											<span style="color:#aac27f"><span style="color:#ffa400">＂</span>一點點醉調皮！<span style="color:#ffa400">＂</span></span>
										</div>
									</div>
                  <div class="txt5 pp">
										<div class="title"><img src="images/s3-15.png"></div>
										<div class="txt">
											酸甜的滋味中品嚐得到清新鳳梨香氣，<br>
											乳酸溫潤、氣泡輕快!<br>
											鳳梨加多多，果汁多多、幸運多多<br>
											<span style="color:#ffc600"><span style="color:#ffc600">＂</span>一點點醉旺來<span style="color:#ffc600">＂</span></span>
										</div>
									</div>
								</div>
							</div>

							<div class="corner">
								<div class="corner1 qq"><img src="images/s3-5.png"></div>
								<div class="corner2 qq"><img src="images/s3-6.png"></div>
								<div class="corner3 qq"><img src="images/s3-7.png"></div>
								<div class="corner3 qq"><img src="images/s3-11.png"></div>
								<div class="corner3 qq"><img src="images/s3-14.png"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- block4 -->
			<div class="section xx">
				<div class="s4">
					<div class="content">
						<ul>
							<!-- <li><img src="images/link-logo-9.jpg"></li> -->
							<li><img src="images/link-logo-2.jpg"></li>
							<li><img src="images/link-logo-1.jpg"></li>
							<li><img src="images/link-logo-5.jpg"></li>
							<li class="clear"></li>
							<li><img src="images/link-logo-4.jpg"></li>
							<li><img src="images/link-logo-8.jpg"></li>
							<li><img src="images/link-logo-7.jpg"></li>
							<li><img src="images/link-logo-3.jpg"></li>
						</ul>
						<dl>
							<dt>以下通路都買的到：</dt>
							<dd>大潤發、全家、全聯、<br class="br">家樂福、新光三越、微風超市、楓康超市、<br>愛買、遠東SOGO、c!ty'super、JASONS Market Place</dd>
							<br>
							<dt>實際販售品項口味請依各通路為準</dt>
						</dl>

					</div>

				</div>
			</div>
		</div>

		<footer>
			<div>禁止酒駕 <img src="images/tag01.png"> 飲酒過量 有害健康</div>
			<!-- <div class="mobile"><img src="images/waring.gif"></div> -->
		</footer>
	</div>

<script src="js/bubble.js"></script>
<!-- scrollreveal JS File -->
<script src="js/scrollreveal.min.js"></script>
<script src="js/scrollreveal_control.js"></script>
<!--  -->
<script src="fullpage/fullpage.js"></script>
<script type="text/javascript">
	//什麼是沙瓦跳出框
	$(function(){
		var $what=$('.what'),
			$what_lightbox=$('.what-lightbox'),
			$close=$('.icon-icon09')

		$what.click(function(e){
			// console.log('ddd')
			$(this).hide();
			$what_lightbox.fadeIn();
			e.preventDefault();
		})
		$close.click(function(e){
			// console.log('ddd')
			$what_lightbox.hide();
			$what.show();
			e.preventDefault();
		})
	})
	// 手機版選單開關
	$(function(){
		var _w=$(window).outerWidth();

		function toggleMenu () {
			if(_w<768){
				$('.switch').click(function(){
					$('.menu-mobile').toggle();
					$('header').toggle();
					$('.cover').toggle();
					// console.log('aac')
				})

				$('.icon-icon07').click(function(){
					$('.menu-mobile').toggle();
					$('header').toggle();
					$('.cover').toggle();
				})

				$('#menu li a').click(function(){
					$('.cover').hide();
					$('.menu-mobile').toggle();
					$('header').toggle();
				})
			}else{
				$('.dark').hide();
			}
		}

		toggleMenu()

		$(window).on('resize', function () {
			toggleMenu()
		})
	})

	// 口味介紹切換
	$(function(){
		var $btn=$('.btn').find('li'),
			$pdt=$('.pdt'),
			$pdtx=$pdt.find('.pp'),
			$corner=$('.corner'),
			$cornerx=$corner.find('.qq'),
			$txts=$('.txts'),
			$txtsx=$txts.find('.pp')

		$btn.mouseover(function(e){
			var $this=$(this),
				_index=$this.index()

			$pdtx.eq(_index).show().siblings().css('display','none');
			$txtsx.eq(_index).show().siblings().css('display','none');
			$cornerx.eq(_index).show().siblings().css('display','none');
			// $pdtx.eq(_index).fadeIn().siblings().css('display','none');
			// $txtsx.eq(_index).fadeIn().siblings().css('display','none');
			// $cornerx.eq(_index).fadeIn().siblings().css('display','none');
			$this.find('a').addClass('selected').parent().siblings().find('a').removeClass();
			e.preventDefault();

			console.log(_index);


		})
	})

	$(".tag2").click(function(){
		$("li.straw > a").mouseover();
	})
</script>


<!-- fullpage plugin -->
<script>
    $(document).ready(function(){
    	var s=1;
        //initialising fullpage.js in the jQuery way
        $('#fullpage').fullpage({
            sectionsColor: ['#ffe1e0', '#ffe1e0', '#ffe1e0', '#ffe1e0'],
            anchors: ['firstPage', 'secondPage', '3rdPage', '4rdPage'],
			menu: '#menu',
	        css3: true,
	        navigation:true,
	        navigationPosition: 'left',
	        // responsiveWidth: 768,
	        afterRender: function(){
	        	$("#menu_gotop").hide();
	        },
	        onLeave: function(section, origin, destination, direction){
	        	//using anchorLink
				if(origin.anchor == '4rdPage'){
					$("#menu_gotop").show();
					$("#moveSectionDown").hide();
				}
				else
				{
					$("#menu_gotop").hide();
					$("#moveSectionDown").show();
				}
	        }
        });

        $('#moveSectionDown').click(function(e){
            e.preventDefault();
            $.fn.fullpage.moveSectionDown();
        });

    });

    var setWidth = $(window).width();

    $(window).resize(function() {
    	if(setWidth != $(window).width())
    	{
    		window.location.reload();
    	}


    	// var _w=$(window).outerHeight()
    	// if(_w<768){
	    // 	$('.section').outerHeight(_w)
	    // 	console.log($('.section').outerHeight())

    	// }
	});
</script>

<style type="text/css">
	.ani{
		animation:sour .5s forwards cubic-bezier(.17,.67,.76,1.47);
		animation-delay:3.5s;
	}
	.ani2{
		animation:sour2 .5s forwards cubic-bezier(.17,.67,.76,1.47);
		animation-delay:3.5s;
	}
	.ani3{
		animation:sour3 .5s forwards cubic-bezier(.17,.67,.76,1.47);
		animation-delay:3.5s;
	}

	@keyframes sour{
		0%{
			transform:translateX(0px);
		}
		25%{
			transform:translateX(20px);
		}
		50%{
			transform:translateX(0px);
		}
		75%{
			transform:translateX(10px);
		}
		100%{
			transform:translateX(0px);
		}
	}
	@keyframes sour2{
		0%{
			transform:translateX(0px);
		}
		25%{
			transform:translateX(-20px);
		}
		50%{
			transform:translateX(0px);
		}
		75%{
			transform:translateX(0px);
		}
		100%{
			transform:translateX(0px);
		}
	}
	@keyframes sour3{
		0%{
			transform:translateX(0px);
		}
		25%{
			transform:translateX(-30px);
		}
		50%{
			transform:translateX(0px);
		}
		75%{
			transform:translateX(0px);
		}
		100%{
			transform:translateX(0px);
		}
	}

</style>
</body>
</html>