<?php include('header.php'); ?>
	<nav class="breadcrumbwrap">
		<div class="p-1200">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="index.php">首頁</a></li>
				<li class="breadcrumb-item"><a href="">購物商城</a></li>
				<li class="breadcrumb-item"><a href="">第一層</a></li>
				<li class="breadcrumb-item"><a href="">酒類</a></li>
				<li class="breadcrumb-item"><a href="">大分類A</a></li>
				<li class="breadcrumb-item"><a href="">品牌</a></li>
				<li class="breadcrumb-item active"><a href="#">商品</a></li>
			</ol>
		</div>
	</nav>
	<div id="product">
		<div class="p-1200 page-cart">
			<div class="top-area">
				<div class="pic-wrap">
					<div class="big-pic" data-plugin="big-pic">
						<div class="item"><img src="assets/images/product_1.jpg" alt="*" class="rwd-img"></div>
						<div class="item"><img src="assets/images/product_2.jpg" alt="*" class="rwd-img"></div>
						<div class="item"><img src="assets/images/product_3.jpg" alt="*" class="rwd-img"></div>
						<div class="item"><img src="assets/images/product_1.jpg" alt="*" class="rwd-img"></div>
						<div class="item"><img src="assets/images/product_2.jpg" alt="*" class="rwd-img"></div>
					</div>
					<div class="small-pic" data-plugin="small-pic">
						<div class="item"><img src="assets/images/product_1.jpg" alt="*" class="rwd-img"></div>
						<div class="item"><img src="assets/images/product_2.jpg" alt="*" class="rwd-img"></div>
						<div class="item"><img src="assets/images/product_3.jpg" alt="*" class="rwd-img"></div>
						<div class="item"><img src="assets/images/product_1.jpg" alt="*" class="rwd-img"></div>
						<div class="item"><img src="assets/images/product_2.jpg" alt="*" class="rwd-img"></div>
					</div>
				</div>
				<div class="text-wrap">
					<div class="top-box">
						<div class="type">限量活動</div>
						<div class="title">KEWPIE 美乃滋 500g</div>
					</div>
					<div class="info-box">
						日本美乃滋創始品牌！<br>
						台灣唯一正式代理！<br>
						美乃滋可運用於沾醬、炸物、廣島燒、章魚燒醬；<br>
						混合明太子後作焗烤醬等，用途廣泛！
					</div>
					<div class="function-box">
						<div class="box">
							<div class="title">促銷價</div>
							<div class="main"><span class="new-price">NT$ 2,290</span><span class="old-price">NT$ 2,880</span></div>
						</div>
						<div class="box qty">
							<div class="title">數量</div>
							<div class="main">
								<div class="wan-spinner" data-max="10" data-min="1" data-step="1">
								<a href="javascript:void(0)" class="minus">-</a>
								<input type="text" value="1">
								<a href="javascript:void(0)" class="plus">+</a>
								</div>
							</div>
						</div>
						<div class="box">
							<div class="title">運送方式</div>
							<div class="main">常溫宅配</div>
						</div>
						<div class="box">
							<div class="title">促銷活動</div>
							<div class="main">滿額免運活動名稱<br>滿額折扣活動名稱</div>
						</div>
					</div>
					<div class="button-box mb-2">
						<div class="btn-box-1 w-100">
							<a href="register-info.php" class="button-style">加入購物車</a>
							<a href="register-info.php" class="button-style brown2"><i class="fas fa-dollar-sign"></i> 立即結帳</a>
                        </div>
						<!-- <a name="" type="" class="button-style brown3 text-center" href="javascript::void(0);"><i class="fas fa-shopping-cart"></i> 加入購物車</a>
						<a name="" type="" href="cart.php" class="button-style brown3 text-center"><i class="fas fa-dollar-sign"></i> 立即結帳</a> -->
					</div>
					<div class="share-box">
						<div class="txt">SHARE：</div>
						<ul class="reset">
							<li><a href="#" title="Facebook" class="fb"><i class="fab fa-facebook"></i></a></li>
							<li><a href="#" title="LINE" class="line"><i class="fab fa-line"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
            <ul class="nav nav-pills text-center mb-3">
                <li class="active"><a data-toggle="tab" href="#area1">詳細說明</a></li>
                <li><a data-toggle="tab" href="#area2">品嚐推薦</a></li>
                <li><a data-toggle="tab" href="#area3">商品資訊</a></li>
            </ul>
			<div class="tab-content">
			  <div class="tab-pane active" id="area1" role="tabpanel">
			  	<div class="middle-area">
				  	<div class="p-subject">詳細說明</div>
						<div class="edit-wrap">
							<img src="assets/images/bn-2-min.png" alt=""><br><br>
							KEWPIE美乃滋的美味關鍵<br>
							主要成份為植物油、醋、蛋，製作出豐富滋味的美乃滋。<br>
							植物油：脂質是人體熱量的來源，適量攝取有利於脂溶性營養素的吸收。<br>
							醋：選用蘋果汁及麥芽，獨家製作出美乃滋專用醋。醋可以防止美乃滋腐壞之外，還可以降低血糖、消除疲勞！<br>
							蛋：嚴選雞蛋，取出蛋黃加入製作，一瓶500g的美乃滋裡就富含四顆新鮮蛋黃，營養價值相當高！助於記憶力和學習力的提升。<br>
							美乃滋的運用<br>
							美乃滋主要用於沾醬，炸物、廣島燒、章魚燒沾醬；混合明太子後做焗烤醬等，用途廣泛!
						</div>
					</div>
			  </div>
			  <div class="tab-pane" id="area2" role="tabpanel">
			  	<div class="middle-area">
						<div class="p-subject">品嚐推薦</div>
						<div class="edit-wrap">
							<img src="assets/images/bn-min.png" alt="">
						</div>
					</div>
			  </div>
			  <div class="tab-pane" id="area3" role="tabpanel">
			  	<div class="middle-area">
						<div class="p-subject">商品資訊</div>
						<div class="edit-wrap">
							KEWPIE美乃滋的美味關鍵<br>
							主要成份為植物油、醋、蛋，製作出豐富滋味的美乃滋。<br>
							植物油：脂質是人體熱量的來源，適量攝取有利於脂溶性營養素的吸收。<br>
							醋：選用蘋果汁及麥芽，獨家製作出美乃滋專用醋。醋可以防止美乃滋腐壞之外，還可以降低血糖、消除疲勞！<br>
							蛋：嚴選雞蛋，取出蛋黃加入製作，一瓶500g的美乃滋裡就富含四顆新鮮蛋黃，營養價值相當高！助於記憶力和學習力的提升。<br>
							美乃滋的運用<br>
							美乃滋主要用於沾醬，炸物、廣島燒、章魚燒沾醬；混合明太子後做焗烤醬等，用途廣泛!<br><br>
							KEWPIE美乃滋的美味關鍵<br>
							主要成份為植物油、醋、蛋，製作出豐富滋味的美乃滋。<br>
							植物油：脂質是人體熱量的來源，適量攝取有利於脂溶性營養素的吸收。<br>
							醋：選用蘋果汁及麥芽，獨家製作出美乃滋專用醋。醋可以防止美乃滋腐壞之外，還可以降低血糖、消除疲勞！<br>
							蛋：嚴選雞蛋，取出蛋黃加入製作，一瓶500g的美乃滋裡就富含四顆新鮮蛋黃，營養價值相當高！助於記憶力和學習力的提升。<br>
							美乃滋的運用<br>
							美乃滋主要用於沾醬，炸物、廣島燒、章魚燒沾醬；混合明太子後做焗烤醬等，用途廣泛!
						</div>
					</div>
			  </div>
			</div>
			<div class="middle-area">
				<div class="p-subject">注意事項</div>
				<div class="edit-wrap">
					<img src="assets/images/logo-min.png" alt=""><br><br>
					KEWPIE美乃滋的美味關鍵<br>
					主要成份為植物油、醋、蛋，製作出豐富滋味的美乃滋。<br>
					植物油：脂質是人體熱量的來源，適量攝取有利於脂溶性營養素的吸收。<br>
					醋：選用蘋果汁及麥芽，獨家製作出美乃滋專用醋。醋可以防止美乃滋腐壞之外，還可以降低血糖、消除疲勞！
				</div>
			</div>
		</div>
		<div class="intro-wrap">
			<div class="p-1200">
				<div class="subject">相關商品</div>
				<div class="intro-slider">
					<div data-plugins="intro-slick">
						<!-- 01 -->
						<div href="product_view.php" class="item">
							<div class="box">
								<div class="img"><img src="assets/images/product_1.jpg" alt="*" class="rwd-img"></div>
								<div class="main">
									<a href="product_view.php" class="title">SOUR3沙瓦 芭樂柳橙風味 350ml（24入）</a>
									<div class="price">
										<div class="new">NT$1,050</div>
										<div class="old">NT$1,280</div>
									</div>
									<div class="more">
										<button name="" type="" class="button-style brown">加入購物車</button>
									</div>
								</div>
							</div>
						</div>
						<!-- /01 -->
						<div class="item">
							<div class="box">
								<div class="img"><img src="assets/images/product_2.jpg" alt="*" class="rwd-img"></div>
								<div class="main">
									<a href="product_view.php" class="title">SOUR3沙瓦 芭樂柳橙風味 350ml（24入）</a>
									<div class="price">
										<div class="new">NT$1,050</div>
										<div class="old">NT$1,280</div>
									</div>
									<div class="more">
										<button name="" type="" class="button-style brown">加入購物車</button>
									</div>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="box">
								<div class="img"><img src="assets/images/product_3.jpg" alt="*" class="rwd-img"></div>
								<div class="main">
									<a href="product_view.php" class="title">SOUR3沙瓦 芭樂柳橙風味 350ml（24入）</a>
									<div class="price">
										<div class="new">NT$1,050</div>
										<div class="old">NT$1,280</div>
									</div>
									<div class="more">
										<button name="" type="" class="button-style brown">加入購物車</button>
									</div>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="box">
								<div class="img"><img src="assets/images/product_1.jpg" alt="*" class="rwd-img"></div>
								<div class="main">
									<a href="product_view.php" class="title">SOUR3沙瓦 芭樂柳橙風味 350ml（24入）</a>
									<div class="price">
										<div class="new">NT$1,050</div>
										<div class="old">NT$1,280</div>
									</div>
									<div class="more">
										<button name="" type="" class="button-style brown">加入購物車</button>
									</div>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="box">
								<div class="img"><img src="assets/images/product_2.jpg" alt="*" class="rwd-img"></div>
								<div class="main">
									<a href="product_view.php" class="title">SOUR3沙瓦 芭樂柳橙風味 350ml（24入）</a>
									<div class="price">
										<div class="new">NT$1,050</div>
										<div class="old">NT$1,280</div>
									</div>
									<div class="more">
										<button name="" type="" class="button-style brown">加入購物車</button>
									</div>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="box">
								<div class="img"><img src="assets/images/product_3.jpg" alt="*" class="rwd-img"></div>
								<div class="main">
									<a href="product_view.php" class="title">SOUR3沙瓦 芭樂柳橙風味 350ml（24入）</a>
									<div class="price">
										<div class="new">NT$1,050</div>
										<div class="old">NT$1,280</div>
									</div>
									<div class="more">
										<button name="" type="" class="button-style brown">加入購物車</button>
									</div>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="box">
								<div class="img"><img src="assets/images/product_1.jpg" alt="*" class="rwd-img"></div>
								<div class="main">
									<a href="product_view.php" class="title">SOUR3沙瓦 芭樂柳橙風味 350ml（24入）</a>
									<div class="price">
										<div class="new">NT$1,050</div>
										<div class="old">NT$1,280</div>
									</div>
									<div class="more">
										<button name="" type="" class="button-style brown">加入購物車</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php include('footer.php'); ?>
<script src="assets/js/wan-spinner.js"></script>
<script>
$(function() {

	var album = $('[data-plugin="big-pic"]');
  var pager = $('[data-plugin="small-pic"]');

  album.slick({
    dots: false,
    fade: true,
    arrows: false,
    draggable: false,
    swipe: false,
    cssEase: 'cubic-bezier(0,.4,.4,1)',
    asNavFor: pager
  });

  pager.slick({
    dots: false,
    arrows: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    swipeToSlide: true,
    focusOnSelect: true,
    asNavFor: album
  });

  $('.wan-spinner').each(function(i,element) {
    var elem = $(element);
    var min = elem.data('min');
    var max = elem.data('max');
    var step = elem.data('step');
    var start = elem.find('input').val();
    elem.WanSpinner({
      start: start,
      step: step,
      minValue: min,
      maxValue: max,
      valueChanged: function() {
        return false;
      }
    });
  });

	$('[data-plugins="intro-slick"]').slick({
	  infinite: true,
	  slidesToShow: 4,
	  slidesToScroll: 1,
	  autoplay: true,
	  arrows: false,
	  dots: true,
	  responsive: [
	    {
	      breakpoint: 1281,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 1
	      }
	    },
	    {
	      breakpoint: 768,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2
	      }
	    }
	  ]
	});
});
</script>