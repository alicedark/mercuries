<!DOCTYPE html>
<html lang="zh-Hant">
<head>
<meta charset="utf-8">
<meta name="keywords" content="三商食品 :: 此為校稿頁面">
<meta name="description" content="三商食品 :: 此為校稿頁面">
<title>三商食品 :: 此為校稿頁面</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="format-detection" content="telephone=no">
<!-- 網頁過期時間: 立即 -->
<meta http-equiv="expires" content="0">
<!-- ​​Pragma (cache模式) -->
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">

<base href="./" data-theme="" data-lang="">
<link href="favicon.ico" rel="icon">
<link href="favicon.ico" rel="shortcut icon">
<link href="apple-touch-icon.png" rel="apple-touch-icon">
<link href="https://fonts.googleapis.com/css2?family=Noto+Sans+TC:wght@300;400;500;700&display=swap" rel="stylesheet">
<link href="assets/css/bootstrap-3.3.7.min.css" rel="stylesheet">
<link href="assets/css/fontawesome-5.4.1-all.min.css" rel="stylesheet">
<link href="assets/css/aos.css" rel="stylesheet">
<link href="assets/css/custom.css?t=<?=time()?>" rel="stylesheet">
<link href="assets/css/common.css?t=<?=time()?>" rel="stylesheet">
<link href="assets/css/plugin-slick.min.css" rel="stylesheet">
<!--[if lt IE 9]>
<script src="assets/js/plugins/html5shiv-3.7.3.min.js"></script>
<script src="assets/js/plugins/respond-1.4.2.min.js"></script>
<![endif]-->
</head>
<body>

<div class="out-wrap">
<!-- Page Header Start -->
	<header id="header">
		<h1 class="logo"><a href="#" title="三商食品 :: 此為校稿頁面"><img class="logo-img" src="assets/images/logo-min.png"></a></h1>
		<div id="nav-icon"><span></span><span></span><span></span></div>
		<nav id="nav">
			<ul class="reset">
				<li class="has-child">
					<a href="#" title="">關於我們</a>
					<div class="sub-nav">
						<ul class="reset">
							<li><a href="about.php" title="企業介紹">企業介紹</a></li>
							<li><a href="about2.php" title="飲食店樣向">飲食店樣向</a></li>
						</ul>
					</div>
				</li>
				<li class="has-child">
					<a href="javascript::void(0);" title="情報誌">情報誌</a>
					<div class="sub-nav">
						<ul class="reset">
							<li><a href="magazine_list.php" title="繁盛店情報誌">繁盛店情報誌</a></li>
							<li><a href="article_list.php" title="SOUR3 季節酒單">SOUR3 季節酒單</a></li>
							<li><a href="article_list.php" title="熟手新知">熟手新知</a></li>
							<li><a href="location.php" title="品嚐據點">品嚐據點</a></li>
						</ul>
					</div>
				</li>
				<li><a href="product_list2_b.php" title="餐飲用產品">餐飲用產品</a></li>
				<li><a href="contact.php" title="聯絡我們">聯絡我們</a></li>
				<li><a href="product_list.php" title="購物商城">購物商城</a></li>
			</ul>
		</nav>
		<div class="top-sub">
			<a href="cart-inquery.php" title="詢問單" class="inquiry-box">
				<span class="icon inquiry"></span>
				<span class="txt">詢問單</span>
			</a>
			<a href="cart.php" title="購物車" class="cart-box"><i class="fas fa-shopping-cart"></i></a>
			<a href="login.php" title="會員登入" class="member-box"><i class="fas fa-user"></i></a>
		</div>
	</header>
	<!-- Page Header End -->