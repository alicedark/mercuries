<?php include('header.php'); ?>
	<nav class="breadcrumbwrap">
		<div class="p-1200">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="index.php">首頁</a></li>
				<li class="breadcrumb-item"><a href="">購物商城</a></li>
				<li class="breadcrumb-item"><a href="">第一層</a></li>
				<li class="breadcrumb-item"><a href="">酒類</a></li>
				<li class="breadcrumb-item"><a href="">大分類A</a></li>
				<li class="breadcrumb-item"><a href="">品牌</a></li>
				<li class="breadcrumb-item active"><a href="#">餐飲用產品內頁</a></li>
			</ol>
		</div>
	</nav>
	<div id="product" class="templete-c">
		<div class="p-1200">
			<div class="top-area">
				<div class="pic-wrap">
					<div class="big-pic" data-plugin="big-pic">
						<div class="item"><img src="assets/images/p4-min.png" alt="*" class="rwd-img"></div>
						<div class="item"><img src="assets/images/p4-min.png" alt="*" class="rwd-img"></div>
						<div class="item"><img src="assets/images/p4-min.png" alt="*" class="rwd-img"></div>
						<div class="item"><img src="assets/images/p4-min.png" alt="*" class="rwd-img"></div>
						<div class="item"><img src="assets/images/p4-min.png" alt="*" class="rwd-img"></div>
						<div class="item"><img src="assets/images/p4-min.png" alt="*" class="rwd-img"></div>
					</div>
					<div class="small-pic">
						<div data-plugin="small-pic">
							<div class="item"><img src="assets/images/p4-min.png" alt="*" class="rwd-img"></div>
							<div class="item"><img src="assets/images/p4-min.png" alt="*" class="rwd-img"></div>
							<div class="item"><img src="assets/images/p4-min.png" alt="*" class="rwd-img"></div>
							<div class="item"><img src="assets/images/p4-min.png" alt="*" class="rwd-img"></div>
							<div class="item"><img src="assets/images/p4-min.png" alt="*" class="rwd-img"></div>
							<div class="item"><img src="assets/images/p4-min.png" alt="*" class="rwd-img"></div>
						</div>
					</div>
				</div>
				<div class="text-wrap">
					<div class="top-box">
						<div class="title">KEWPIE 美乃滋 500g</div>
						<div class="type">商品編號: L8033</div>
					</div>
					<div class="info-box">
						<div class="subject">商品特色</div>
						日本美乃滋創始品牌！台灣唯一正式代理！<br><br>
						美乃滋可運用於沾醬、炸物、廣島燒、章魚燒醬；混合明太子後作焗烤醬等，用途廣泛！
					</div>
					<div class="button-box">
						<a href="cart.php" title="營業用詢價" class="button-style brown2">營業用詢價</a>
					</div>
					<div class="share-box">
						<div class="txt">SHARE：</div>
						<ul class="reset">
							<li><a href="#" title="Facebook" class="fb"><i class="fab fa-facebook"></i></a></li>
							<li><a href="#" title="LINE" class="line"><i class="fab fa-line"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="middle-area">
				<div class="p-subject">詳細說明</div>
				<div class="edit-wrap">
					KEWPIE美乃滋的美味關鍵<br><br>
					主要成份為植物油、醋、蛋，製作出豐富滋味的美乃滋。<br>
					植物油：脂質是人體熱量的來源，適量攝取有利於脂溶性營養素的吸收。<br>
					醋：選用蘋果汁及麥芽，獨家製作出美乃滋專用醋。醋可以防止美乃滋腐壞之外，還可以降低血糖、消除疲勞！<br>蛋：嚴選雞蛋，取出蛋黃加入製作，一瓶500g的美乃滋裡就富含四顆新鮮蛋黃，營養價值相當高！助於記憶力和學習力的提升。<br><br>
					美乃滋的運用<br>
					美乃滋主要用於沾醬，炸物、廣島燒、章魚燒沾醬；混合明太子後做焗烤醬等，用途廣泛!<br><br>
					<img src="assets/images/shopping-index-bn3-min.png" alt="" width="100%"><br><br><br>
				</div>
				<div class="p-subject">詳細說明</div>
				<div class="edit-wrap">
					※更多果汁沙瓦請至☞<a href="">「SOUR3沙瓦系列」</a><br><br><br>
				</div>
				<div class="p-subject2">商品資訊</div>
				<div class="edit-wrap">
					．容量規格：350ml/罐裝（24入）<br><br>
						．酒精度數：3%<br><br>
						．果汁含量：25%<br><br>
						．主要成分：桃子果汁、果糖葡萄糖液糖、釀造酒精、調味劑(無水檸檬酸)、香料、抗氧化劑(維生素C)<br><br>
						．產地：日本
				</div>
				<div class="button-box">
						<a href="cart.php" title="營業用詢價" class="button-style brown2">營業用詢價</a>
					</div>
			</div>
			<div class="middle-area">
					<div class="tab-box">
						<ul class="reset nav-tabs" role="tablist">
						<li class="nav-item active">
							<a class="nav-link" data-toggle="tab" href="#area1" role="tab" aria-selected="true">品嚐推薦</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#area2" role="tab" aria-selected="false">商品資訊</a>
						</li>
						</ul>
					</div>
					<div class="tab-content">
					<div class="tab-pane active" id="area1" role="tabpanel">
							<div class="edit-wrap">
								．容量規格：500g/瓶<br><br>
								．主要成分：食用植物油(大豆)、蛋黃、釀造醋(蘋果)、食鹽、L-麩酸鈉、香辛料(胡椒)、香辛料抽出物(辣椒粉)<br><br>
								．保存期限：未開封12個月<br><br>
								．保存方式：開封後請放冰箱冷藏保存，並於1個月內使用完畢<br><br>
								．產地：日本兵庫縣
							</div>
					</div>
					<div class="tab-pane" id="area2" role="tabpanel">
							<div class="edit-wrap">
								KEWPIE美乃滋的美味關鍵<br>
								主要成份為植物油、醋、蛋，製作出豐富滋味的美乃滋。<br>
								植物油：脂質是人體熱量的來源，適量攝取有利於脂溶性營養素的吸收。<br>
								醋：選用蘋果汁及麥芽，獨家製作出美乃滋專用醋。醋可以防止美乃滋腐壞之外，還可以降低血糖、消除疲勞！<br>
								蛋：嚴選雞蛋，取出蛋黃加入製作，一瓶500g的美乃滋裡就富含四顆新鮮蛋黃，營養價值相當高！助於記憶力和學習力的提升。<br>
								美乃滋的運用<br>
								美乃滋主要用於沾醬，炸物、廣島燒、章魚燒沾醬；混合明太子後做焗烤醬等，用途廣泛!<br><br>
								KEWPIE美乃滋的美味關鍵<br>
								主要成份為植物油、醋、蛋，製作出豐富滋味的美乃滋。<br>
								植物油：脂質是人體熱量的來源，適量攝取有利於脂溶性營養素的吸收。<br>
								醋：選用蘋果汁及麥芽，獨家製作出美乃滋專用醋。醋可以防止美乃滋腐壞之外，還可以降低血糖、消除疲勞！<br>
								蛋：嚴選雞蛋，取出蛋黃加入製作，一瓶500g的美乃滋裡就富含四顆新鮮蛋黃，營養價值相當高！助於記憶力和學習力的提升。<br>
								美乃滋的運用<br>
								美乃滋主要用於沾醬，炸物、廣島燒、章魚燒沾醬；混合明太子後做焗烤醬等，用途廣泛!
							</div>
					</div>
					</div>
				</div>
			</div>
			<div class="intro-wrap">
				<div class="p-1200">
					<div class="subject">相關商品</div>
					<div class="product-list2">
						<ul class="reset" data-plugins="product-list2">
							<li>
								<div class="item">
									<a href="product_view2.php" title="">
										<span class="img"><img src="assets/images/p1-min.png" alt=""></span>
										<span class="main">
											<span class="title mb-3">丸金 特級淡口醬油 1000ml</span>
											<!-- <span class="line"><span></span></span>
											<span class="price no-old"><span class="new">NT$ 180</span></span> -->
										</span>
									</a>
								</div>
							</li>
							<li>
								<div class="item">
									<a href="product_view2.php" title="">
										<span class="img"><img src="assets/images/p2-min.png" alt=""></span>
										<span class="main">
											<span class="title mb-3">即期良品促銷- 富澤商店 烘焙材料烘焙材料</span>
											<!-- <span class="line"><span></span></span>
											<span class="price"><span class="old">NT$ 180</span><span class="new">NT$ 180</span></span> -->
										</span>
									</a>
								</div>
							</li>
							<li>
								<div class="item">
									<a href="product_view2.php" title="">
										<span class="img"><img src="assets/images/p3-min.png" alt=""></span>
										<span class="main">
											<span class="title mb-3">丸金 特級淡口醬油 1000ml</span>
											<!-- <span class="line"><span></span></span>
											<span class="price no-old"><span class="new">NT$ 180</span></span> -->
										</span>
									</a>
								</div>
							</li>
							<li>
								<div class="item">
									<a href="product_view2.php" title="">
										<span class="img"><img src="assets/images/p1-min.png" alt=""></span>
										<span class="main">
											<span class="title mb-3">丸金 特級淡口醬油 1000ml</span>
											<!-- <span class="line"><span></span></span>
											<span class="price no-old"><span class="new">NT$ 180</span></span> -->
										</span>
									</a>
								</div>
							</li>
							<li>
								<div class="item">
									<a href="product_view2.php" title="">
										<span class="img"><img src="assets/images/p2-min.png" alt=""></span>
										<span class="main">
											<span class="title mb-3">即期良品促銷- 富澤商店 烘焙材料烘焙材料</span>
											<!-- <span class="line"><span></span></span>
											<span class="price"><span class="old">NT$ 180</span><span class="new">NT$ 180</span></span> -->
										</span>
									</a>
								</div>
							</li>
							<li>
								<div class="item">
									<a href="product_view2.php" title="">
										<span class="img"><img src="assets/images/p3-min.png" alt=""></span>
										<span class="main">
											<span class="title mb-3">丸金 特級淡口醬油 1000ml</span>
											<!-- <span class="line"><span></span></span>
											<span class="price no-old"><span class="new">NT$ 180</span></span> -->
										</span>
									</a>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>


<?php include('footer.php'); ?>
<script src="assets/js/wan-spinner.js"></script>
<script>
$(function() {

	var album = $('[data-plugin="big-pic"]');
  var pager = $('[data-plugin="small-pic"]');

  album.slick({
    dots: false,
    fade: true,
    arrows: true,
    draggable: false,
    swipe: false,
    cssEase: 'cubic-bezier(0,.4,.4,1)',
    asNavFor: pager
  });

  pager.slick({
    dots: false,
    arrows: false,
    slidesToShow: 5,
    slidesToScroll: 1,
    swipeToSlide: true,
    focusOnSelect: true,
    asNavFor: album,
    vertical: true,
	  responsive: [
	    {
	      breakpoint: 1281,
	      settings: {
	        vertical: false,
			    slidesToShow: 5,
			    slidesToScroll: 1
	      }
	    }
	  ]
  });

	$('[data-plugins="product-list2"]').slick({
	  infinite: true,
	  slidesToShow: 4,
	  slidesToScroll: 1,
	  autoplay: true,
	  arrows: true,
	  dots: false,
	  responsive: [
	    {
	      breakpoint: 1281,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 1
	      }
	    },
	    {
	      breakpoint: 768,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2
	      }
	    }
	  ]
	});
});
</script>