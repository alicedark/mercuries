
	<?php include('header.php'); ?>
	<div class="page-account" data-aos="fade-in">
        <nav class="breadcrumbwrap">
            <div class="container">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.php">首頁</a></li>
                    <li class="breadcrumb-item active"><a href="">會員登入</a></li>
                </ol>
            </div>
        </nav>
        <h1 class="title-page">會員登入</h1>
        <div class="container" style="margin-bottom: 40px;">
            <div class="row">
                <div class="col-sm-6">
                    <form>
                        <div class="box mt-0 p-subject sm">
                            <h3 class="tw">若您已是會員，請輸入帳號密碼登入。</h3>
                        </div>
                        <hr>
                        <!-- <p class="mb-4">
                            若您已是會員，請輸入帳號密碼登入。
                        </p> -->
                        <div class="form-group mb-4">
                            <label for="exampleInputEmail1" style="margin-bottom: 1rem;">帳號</label>
                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="請輸入會員帳號 (email)">
                        </div>
                        <div class="form-group mb-4">
                            <label for="exampleInputPassword1">密碼</label>
                            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="請輸入會員密碼">
                        </div>

                        <div class="checkbox mb-4" style="display:flex; justify-content: space-between;">
                            <label>
                                <input type="checkbox" value="remember-me"> 記住我
                            </label>

                            <a href="forget-password.php" class="text-main">忘記密碼？</a>
                        </div>
                        <div class="btn-box-1">
                            <a href="account.php" class="button-style brown2">登入</a>
                            <!-- <button type="submit"  class="button-style brown2">登入</button> -->
                        </div>

                    </form>
                </div>
                <!-- 社群登入 -->
                <div class="col-sm-6">
                    <div class="box mt-0 p-subject sm">
                        <h3 class="tw">社群登入</h3>
                    </div>
                    <!-- <h3 class="title-page">社群登入</h3> -->
                    <hr>
                    <div class="mb-5">
                        <a href="javascript:void(0);" id="facebook" class="btn btn-facebook">
                            <i class="fab fa-facebook-f"></i>&nbsp;&nbsp;Facebook
                        </a>
                        <a href="javascript:void(0);" id="google" class="btn btn-google">
                        <i class="fab fa-google"></i>&nbsp;&nbsp;Google
                        </a>
                    </div>
                    <div>
                        <div class="box mt-0 p-subject sm">
                            <h3 class="tw">若您還不是會員，歡迎加入我們的行列</h3>
                        </div>
                        <!-- <h3 class="title-page">若您還不是會員，歡迎加入我們的行列</h3> -->
                        <hr>
                        <!-- 連結到註冊頁 -->
                        <p class="mb-4">如果您還不是會員，您可以在此建立帳戶。加入會員後可享有更快捷的購物，可隨時查看訂單狀態和訂購記錄，以及更多的會員專屬服務。</p>
                        <div class="btn-box-1">
                            <a href="register-info.php" class="button-style">加入會員</a>
                        </div>
                        <!-- <a href="" class="btn btn-main">註冊</a> -->
                    </div>
                </div>
            </div>
        </div>
    </div>

<!-- my footer start -->

<?php include('footer.php'); ?>