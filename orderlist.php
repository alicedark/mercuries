<?php include('header.php'); ?>
<div data-aos="fade-in">
  <nav class="breadcrumbwrap">
      <div class="container">
          <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="index.php">首頁</a></li>
              <li class="breadcrumb-item"><a href="">會員中心</a></li>
              <li class="breadcrumb-item active"><a href="">詢問單資訊</a></li>
          </ol>
      </div>
  </nav>
  <h1 class="title-page">詢問單資訊</h1>

  <div class="container page-account">
    <div class="row">
      <div class="col-xs-12">
        <ul class="list mb-4 mb-text-small">
          <li><b>商品編號</b> 1022225400</li>
          <li><b>活動代號：</b> mercuries1111</li>
          <li><b>統一編號：</b> 000102222</li>
          <!-- <li><b>付款方式：</b> 銀行轉帳</li> -->
          <li><b>運送方式：</b> 自取</li>
          <li><b>收件地址：</b> 基隆市, 仁愛區 200, address</li>
          <li><b>運送地址：</b> 基隆市, 仁愛區 200, address</li>
        </ul>
        <h5 class="p-subject text-main-dark">訂單詳情</h5>
        <hr>
        <!-- <div class="title-line w-100"></div> -->
        <table class="table table-bordered table-hover mb-text-small">
          <thead class="text-second">
            <tr class="active">
              <td class="text-nowrap">品名</td>
              <td class="text-nowrap">商品規格</td>
              <td class="text-nowrap">數量</td>
              <td class="hidden-xs">價格</td>
              <td class="text-nowrap">小計</td>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td class="text-left">
                <a href="#">商品名稱商品名稱商品名稱商品名稱商品名稱1</a>
              </td>
              <td class="text-left"><a href="#">test1</a></td>
              <td class="text-left">1</td>
              <td class="text-left hidden-xs">NT $1,000</td>
              <td class="text-left">NT $1,000</td>
            </tr>
          </tbody>
          <tfoot class="tfoot-borderless">
            <tr>
              <td class="hidden-xs" colspan="3" rowspan="5"></td>
              <td class="visible-xs" colspan="2" rowspan="5"></td>
              <th class="py-1 text-nowrap"><b>商品金額</b></th>
              <td class="py-1 text-nowrap">NT $1,000</td>
            </tr>
            <tr>
              <th class="py-1 text-nowrap"><b>自取</b></th>
              <td class="py-1 text-nowrap">NT $2,00</td>
            </tr>
            <tr>
              <th class="py-1 text-nowrap"><b>運費折抵</b></th>
              <td class="py-1 text-nowrap">- NT $200</td>
            </tr>
            <tr>
              <th class="py-1 text-nowrap"><b>活動折抵</b></th>
              <td class="py-1 text-nowrap">- NT $120</td>
            </tr>
            <tr>
              <th class="py-1 text-nowrap"><b>訂單總計</b></th>
              <td class="py-1 text-nowrap">NT $1,200</td>
            </tr>
          </tfoot>
        </table>


        <h5 class="p-subject text-main-dark">詢問單紀錄</h5>
        <!-- <h5 class="p-subject text-main-dark">訂單紀錄</h5> -->
        <hr>
        <div class="table-responsive mb-text-small mb-5">

          <table class="table table-bordered table-hover">
            <thead class="text-second">
              <tr class="active">
                <td class="text-left">訂單日期</td>
                <!-- 詢問單狀態 (or 訂單狀態) -->
                <td class="text-left">詢問單狀態</td>
                <td class="text-left">備註</td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td class="text-left">2020-10-27 17:11:49</td>
                <td class="text-left">待付款</td>
                <td class="text-left"><span class="d-inline-block">備註文字備註文字備註文字備註文字</td>
              </tr>
            </tbody>
          </table>
        </div>

        <div class="text-center mb-5">
          <div class="btn-box-1">
            <!-- 狀態若為"已出貨" 不可出現此按鈕 -->
            <!-- <a href="order-history.php" title="返回" class="button-style mr-2">取消訂單</a> -->
            <button type="button" class="button-style back mr-2" data-toggle="modal" data-target=".modal-cancel-order">取消訂單</button>

            <a href="order-history.php" title="返回" class="button-style brown2">返回</a>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>

<!-- 取消訂單: 彈跳視窗 -->
<div class="modal fade modal-cancel-order" tabindex="-1" role="dialog">
  <form class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <p>您確定要取消訂單?</p>
      </div>
      <div class="modal-footer">
        <a type="button" class="btn btn-default" data-dismiss="modal">返回</a>

        <!-- <a class="btn btn-main">確定</a> -->
        <button class="btn btn-danger">確定</button>
      </div>
    </div>
  </form>
</div>

<?php include('footer.php'); ?>
