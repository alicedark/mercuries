<?php include('header.php'); ?>
<div data-aos="fade-in">
  <nav class="breadcrumbwrap">
      <div class="container">
          <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="index.php">首頁</a></li>
              <li class="breadcrumb-item"><a href="">會員中心</a></li>
              <li class="breadcrumb-item active"><a href="">地址簿</a></li>
          </ol>
      </div>
  </nav>
  <h1 class="title-page">地址簿</h1>
  <div class="container page-account">
  <!-- 導覽列 BreadCrumb -->
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2">
        <div class="text-right mb-3">
            <a href="address-form.php" title="新地址" class="btn-main btn-address">+ 新地址</a>
        </div>
        <div class="addr-list mb-5">
          <ul class="list-unstyled">
            <li class="clearfix">
              <a class="addr-list-item" href="address-form.php">
                <!-- 預設 -->
                <span class="badge bg-second">預設</span>
                王小名, 基隆市仁愛區 200, address
              </a>

              <div class="btns pull-right" style="display: inline-block;">
                <!-- Edit -->
                <a href="address-form.php" class="btn btn-icon btn-main-dark"><i class="fa fa-edit"></i></a>
                <!-- Delete -->
                <a href="#" class="btn btn-icon btn-main-dark"><i class="fas fa-trash-alt"></i></a>
              </div>
            </li>

            <li class="clearfix">
              <a class="addr-list-item" href="address-form.php">
                王小名, 基隆市仁愛區 200, address
              </a>

              <div class="btns pull-right"  style="display: inline-block;">
                <!-- Edit -->
                <a href="address-form.php" class="btn btn-icon btn-main-dark"><i class="fa fa-edit"></i></a>
                <!-- Delete -->
                <a href="#" class="btn btn-icon btn-main-dark"><i class="fas fa-trash-alt"></i></a>
              </div>
            </li>
          </ul>
        </div>
        <div class="text-center mb-5">
          <div class="btn-box-1">
            <a href="account.php" title="返回" class="button-style back mr-2">返回</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php include('footer.php'); ?>