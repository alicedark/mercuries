<?php include('header.php'); ?>
<div data-aos="fade-in">	
  <nav class="breadcrumbwrap">
      <div class="container">
          <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="index.php">首頁</a></li>
              <li class="breadcrumb-item"><a href="">購物車</a></li>
              <li class="breadcrumb-item active"><a href="">結帳</a></li>
          </ol>
      </div>
  </nav>
	<h1 class="title-page">結帳</h1>

  <div class="container page-cart">

    <!-- 進度條 -->
    <div class="w-100">
      <div class="stepper-container mb-5 mt-3">
        <div class="stepper">
          <div class="icon-wrap"><i class="fas fa-shopping-cart"></i></div>
          <div class="label-wrap">
            <div class="title">Step 1</div>
            <div class="desc">訂單明細</div>
          </div>
        </div>

        <div class="stepper-arrow">
          <i class="fas fa-arrow-right"></i>
        </div>

        <div class="stepper active">
          <div class="icon-wrap"><i class="fas fa-money-check"></i></div>
          <div class="label-wrap">
            <div class="title">Step 2</div>
            <div class="desc">訂單明細確認 & <br>付款、填寫資料</div>
          </div>
        </div>

        <div class="stepper-arrow">
          <i class="fas fa-arrow-right"></i>
        </div>

        <div class="stepper">
          <div class="icon-wrap"><i class="fas fa-user-check"></i></div>
          <div class="label-wrap">
            <div class="title">Step 3</div>
            <div class="desc">訂單完成</div>
          </div>
        </div>
      </div>
    </div>


    <form action="success.php">
            <div class="row">
              <!-- 收件人欄位 -->
              <div class="col-xs-12">
                <div class="row">
                  <!-- Title -->
                  <div class="col-xs-12">
                    <h5 class="p-subject">結帳資訊</h5>
                    <hr>
                  </div>

                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group mb-4">
                      <label>地址簿 <span class="text-danger">*</span></label>
                      <select class="form-control">
                        <option value="">請選擇</option>
                        <option value="">地址1</option>
                        <option value="">地址2</option>
                      </select>
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group mb-4">
                      <label>姓名<span class="text-danger">*</span></label>
                      <input class="form-control" type="text" />
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group mb-4">
                      <label>E-Mail<span class="text-danger">*</span></label>
                      <input class="form-control" type="email" />
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group mb-4">
                      <label>行動電話 <span class="text-danger">*</span></label>
                      <input class="form-control" type="text" />
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-6">
                    <?php include('_address_combo.php'); ?>
                  </div>

                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group mb-4">
                      <label>備註欄</label>
                      <textarea class="form-control" rows="4" name="comment" placeholder="備註" value=""></textarea>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-lg-6">
                <div class="row">
                  <!-- Title -->

                  <div class="col-xs-12">
                    <h5 class="p-subject">支付方式</h5>
                    <hr>
                  </div>
                  <!-- "訂單" 和 "詢問單" 的付款方式不同，需特別注意 -->
                  <div class="col-xs-12">
                    <div class="form-group mb-4">
                      <br>
                      <div class="radio-inline">
                        <label>
                          <input type="radio" name="gender" id="optionsRadios1" value="m" checked>
                          信用卡
                        </label>
                      </div>

                      <div class="radio-inline">
                        <label>
                          <input type="radio" name="gender" id="optionsRadios2" value="f">
                            銀行轉帳
                        </label>
                      </div>

                      <div class="text-danger w-100">請選擇付款方式</div>
                    </div>
                  </div>
                  <div class="col-xs-12">
                    <h5 class="p-subject">運送方式</h5>
                    <hr>
                    <!-- 後端帶入 -->
                    <div>冷凍宅配</div>
                  </div>
                </div>
              </div>

              <div class="col-lg-6">
                <div class="row">
                  <!-- Title -->

                  <div class="col-xs-12">
                    <h5 class="p-subject">發票</h5>
                    <hr>
                  </div>

                  <div class="col-xs-12">
                    <div class="form-group">
                      <div class="radio-inline">
                        <label>
                          <input type="radio" name="carrierType" id="optionsRadios1" value="twoSheets" checked>
                          二聯
                        </label>
                      </div>

                      <div class="radio-inline">
                        <label>
                          <input type="radio" name="carrierType" id="optionsRadios2" value="threeSheets">
                          三聯
                        </label>
                      </div>
                    </div>
                  </div>
                  <!-- carrierNum -->
                  <div class="col-xs-12 carrierNum mb-4" name="threeSheetsCarrier">
                    <div class="col-xs-6 px-0">
                      <!-- <label>抬頭 <span class="text-danger">*</span></label> -->
                      <input class="form-control" type="text" placeholder="抬頭" />
                    </div>

                    <div class="col-xs-6 pr-0">
                      <!-- <label>統編 <span class="text-danger">*</span></label> -->
                      <input class="form-control" type="text" placeholder="統編" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
    <div class="row pt-4">
      <div class="col-xs-12">
        <h5 class="p-subject text-center mb-4">訂單明細</h5>

        <!-- <div class="title-line w-100"></div> -->
        <table class="table product-table table-bordered">
          <thead>
            <tr class="active">
              <td class="text-nowrap">品名</td>
              <td class="text-nowrap">商品規格</td>
              <td class="">售價</td>
              <td class="text-nowrap">數量</td>
              <td class="text-nowrap">小計</td>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td class="text-center product-img">
                <a href="#">
                  <img src="assets/images/p1-min.png">
                  <br>
                  商品名稱商品名稱商品名稱商品名稱商品名稱1
                </a>
              </td>
              <td class="text-left">
                <span class="mobile-th">商品規格</span>
                <a href="#">test1</a>
              </td>
              <td class="text-right">
                <span class="mobile-th">售價</span>
                NT $1,000
              </td>
              <td class="text-right ">
                <span class="mobile-th">數量</span>
                1
              </td>
              <td class="text-right">
                <span class="mobile-th">小計</span>
                NT $1,000
              </td>
            </tr>
          </tbody>
          <tfoot class="tfoot-borderless">
            <tr>
              <td colspan="3" class="cart-total-empty-td"></td>
              <th class="py-1 text-nowrap cart-total-th"><b>商品金額</b></th>
              <td class="py-1 text-nowrap cart-total-td">NT $1,000</td>
            </tr>
            <tr>
              <td colspan="3" class="cart-total-empty-td"></td>
              <th class="py-1 text-nowrap cart-total-th"><b>折扣活動</b></th>
              <td class="py-1 text-nowrap cart-total-td">- NT $2,00</td>
            </tr>
            <tr>
              <!-- 運費的計算邏輯 -->
              <td colspan="3" class="cart-total-empty-td"></td>
              <th class="py-1 text-nowrap cart-total-th"><b>運費</b></th>
              <td class="py-1 text-nowrap cart-total-td">NT $2,00</td>
            </tr>
            <tr>
              <td colspan="3" class="cart-total-empty-td"></td>
              <th class="py-1 text-nowrap cart-total-th"><b>訂單總計</b></th>
              <td class="py-1 text-nowrap cart-total-td">NT $1,200</td>
            </tr>
          </tfoot>
        </table>
        <p class="text-center"></p>

        <div class="row mb-5">
          <div class="col-xs-12 mb-4" style="display: flex; justify-content: center;">
            <div class="checkbox">
              <label>
                <input type="checkbox">訂閱電子報，以便接收最新優惠及活動訊息</a>
              </label>
            </div>
        </div>

        <div class="text-center mb-5">
          <div class="btn-box-1">
            <a href="cart.php" title="返回" class="button-style back">回上一步</a>
            <a href="cart-success.php" title="返回" class="button-style brown2">結帳</a>
          </div>
        </div>

      </div>

    </div>
  </div>
</div>
<?php include('footer.php'); ?>

<script>
var carrier = $('[name="carrierType"]');
    carrier.change(TypeChange);
//   	carrierNum.attr("disabled", true);

   	carrier.trigger('change');

function TypeChange(){
  var type = $(carrier).filter(":checked").val();
	var carrierNum = $('.carrierNum');

  carrierNum.slideUp();
  $(carrierNum).filter("[name*=" + type + "Carrier]").slideDown();
}
</script>