<?php include('header.php'); ?>
<div class="page-account" data-aos="fade-in">
  <nav class="breadcrumbwrap">
      <div class="container">
          <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="index.php">首頁</a></li>
              <li class="breadcrumb-item"><a href="">會員中心</a></li>
              <li class="breadcrumb-item"><a href="">地址簿</a></li>
              <li class="breadcrumb-item active"><a href="">地址簿新增</a></li>
          </ol>
      </div>
  </nav>
  <h1 class="title-page">地址簿新增</h1>

  <div class="container px-5 pb-lg-5 pb-4">
    <div class="row">
      <div class="col-12">
        <div>
          <form action="success.php">
            <div class="row">
              <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">

                <div class="row">
                  <div class="col-xs-12">
                      <div class="form-group mb-4">
                      <label>姓名 <span class="text-danger">*</span></label>
                      <input class="form-control" type="text" />
                    </div>
                  </div>
                </div>

                <?php include('_address_combo.php'); ?>

                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group mb-4">
                      <label>預設地址? <span class="text-danger">*</span></label>
                      <br>
                      <div class="radio-inline">
                        <label>
                          <input type="radio" name="is_business" id="optionsRadios1" value="y" checked>
                          是
                        </label>
                      </div>

                      <div class="radio-inline">
                        <label>
                          <input type="radio" name="is_business" id="optionsRadios2" value="n">
                          否
                        </label>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="text-center mb-5">
                  <div class="btn-box-1">
                    <a href="addresslist.php" title="返回" class="button-style back mr-2">返回</a>
                    <a href="addresslist.php" title="新地址" class="button-style brown2">送出</a>
                  </div>
                </div>
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<?php include('footer.php'); ?>