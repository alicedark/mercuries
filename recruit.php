
	<?php include('header.php'); ?>
	<div data-aos="fade-in" class="page-recruit">
        <nav class="breadcrumbwrap">
            <div class="container">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.php">首頁</a></li>
                    <li class="breadcrumb-item active"><a href="">人才招募</a></li>
                </ol>
            </div>
        </nav>
        <h1 class="title-page">人才招募</h1>
        <div class="container" style="margin-bottom: 40px;">
            <div class="row">
                <div class="col-sm-12 mt-3">
                    <div class="block">
                        <h4 class="privacy_title">HR聯絡方式</h4>
                        <p>
                            劉小姐 michelle.liu@mlf.com.tw
                        </p>
                    </div>
                    <div class="block">
                        <h4 class="privacy_title">104人力銀行</h4>
                        <p>
                            <a href="https://www.104.com.tw/company/dckis2w?jobsource=jolist_a_relevance" target="_blank">
                            點我招募
                            </a>
                        </p>
                    </div>

                </div>
                <div class="col-sm-8 col-sm-offset-2 mt-5">
                    <img src="assets/images/recruiting.png" class="rwd-img" alt="">
                </div>
            </div>
        </div>
    </div>

<!-- my footer start -->

<?php include('footer.php'); ?>