<?php include('header.php'); ?>
<div data-aos="fade-in">	
  <nav class="breadcrumbwrap">
      <div class="container">
          <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="index.php">首頁</a></li>
              <li class="breadcrumb-item active"><a href="">訂單明細</a></li>
          </ol>
      </div>
  </nav>
	<h1 class="title-page">詢問單</h1>
  <!-- 套後台時移除此備註 -->
  <!-- 購物車 和 詢問單 共用版型 -->
  <p class="text-muted text-center" style="font-size: 14px;">(註:購物車 和 詢問單 共用此版型)</p>

  <div class="container page-cart">
    <div class="row">
      <div class="col-xs-12">
        <div class="w-100">
          <div class="stepper-container mb-5 mt-3">
            <div class="stepper active">
              <div class="icon-wrap"><i class="fas fa-shopping-cart"></i></div>
              <div class="label-wrap">
                <div class="title">Step 1</div>
                <div class="desc">訂單明細</div>
                <!-- <div class="desc">詢問單明細</div> -->
              </div>
            </div>

            <div class="stepper-arrow">
              <i class="fas fa-arrow-right"></i>
            </div>

            <div class="stepper">
              <div class="icon-wrap"><i class="fas fa-money-check"></i></div>
              <div class="label-wrap">
                <div class="title">Step 2</div>
                <div class="desc">訂單明細確認 & <br>付款、填寫資料</div>
                <!-- <div class="desc">詢問單明細確認 & <br>付款、填寫資料</div> -->
              </div>
            </div>

            <div class="stepper-arrow">
              <i class="fas fa-arrow-right"></i>
            </div>

            <div class="stepper">
              <div class="icon-wrap"><i class="fas fa-user-check"></i></div>
              <div class="label-wrap">
                <div class="title">Step 3</div>
                <div class="desc">訂單完成</div>
                <!-- <div class="desc">詢問單完成</div> -->
                </div>
            </div>
          </div>
        </div>

        <ul class="nav nav-pills text-left" style="justify-content: flex-start; margin-bottom: -1px;">
          <li class="active ml-0 mb-0"><a data-toggle="tab" href="#frozen">冷凍</a></li>
          <li class="mb-0"><a data-toggle="tab" href="#normal">常溫</a></li>
        </ul>
        <hr class="mt-0 mb-1">

        <!-- Tab panes -->
        <div class="tab-content">
          <!-- 冷凍宅配: START -->
          <div class="tab-pane fade active in" id="frozen">
            <!-- <div class="title-line w-100"></div> -->
            <table class="table product-table table-bordered">
              <thead class="text-second">
                <tr class="active">
                  <td class="text-nowrap">品名</td>
                  <!-- <td class="text-nowrap">商品規格</td> -->
                  <td class="">售價</td>
                  <td class="text-nowrap">數量</td>
                  <td class="text-nowrap">小計</td>
                  <td class="text-nowrap">刪除</td>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td class="text-center product-img">
                    <a href="#">
                      <img src="assets/images/p1-min.png">
                      <br>
                      商品名稱商品名稱商品名稱商品名稱商品名稱1
                    </a>
                  </td>
                  <!-- <td class="text-left">
                    <span class="mobile-th">商品規格</span>
                    <a href="#">test1</a>
                  </td> -->
                  
                  <td class="text-right text-nowrap">
                    <span class="mobile-th">售價</span>
                    NT $1,000
                  </td>
                  <td class="text-right">
                    <span class="mobile-th">數量</span>

                    <div class="input-number" data-max="10" data-min="1" data-step="1">
                      <a href="javascript:void(0)" class="minus">-</a>
                      <input type="text" value="1">
                      <a href="javascript:void(0)" class="plus">+</a>
                    </div>
                  </td>

                  <td class="text-right text-nowrap">
                    <span class="mobile-th">小計</span>
                    NT $1,000
                  </td>
                  <td class="text-right">
                    <span class="mobile-th">刪除</span>
                    <a class="btn btn-icon cart-del-btn"><i class="fa fa-trash"></i></a>
                  </td>
                </tr>
              </tbody>
              <tfoot class="tfoot-borderless">
                <tr>
                  <td colspan="5" class="py-3 text-nowrap cart-total-td">
                    <div class="text-right pr-sm-3">
                      <h5 class="p-subject text-second my-0 py-0 align-middle d-inline-block">優惠券</h5>
                      <input type="text" class="form-control input-coupon" placeholder="請輸入優惠券代碼">
                      <button class="btn btn-main btn-coupon d-inline-block">使用</button>
                    </div>
                  </td>
                </tr>

                <tr>
                  <td colspan="3" class="cart-total-empty-td"></td>
                  <th class="py-1 text-nowrap cart-total-th"><b>商品小計</b></th>
                  <td class="py-1 text-nowrap cart-total-td">NT $1,000</td>
                </tr>
                <tr>
                  <td colspan="3" class="cart-total-empty-td"></td>
                  <th class="py-1 text-nowrap cart-total-th"><b>折扣活動</b></th>
                  <td class="py-1 text-nowrap cart-total-td">NT $2,00</td>
                </tr>
                <tr>
                  <td colspan="3" class="cart-total-empty-td"></td>
                  <!-- 運費金額由後端帶入(分為冷凍宅配兩種) -->
                  <th class="py-1 text-nowrap cart-total-th"><b>運費</b></th>
                  <td class="py-1 text-nowrap cart-total-td">NT $2,00</td>
                </tr>
                <tr>
                  <td colspan="3" class="cart-total-empty-td"></td>
                  <th class="py-1 text-nowrap cart-total-th"><b>訂單總計</b></th>
                  <td class="py-1 text-nowrap cart-total-td">NT $1,200</td>
                </tr>
              </tfoot>
            </table>
            <p class="text-center"></p>

            <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
              <div class="row pt-4">
                <div class="col-xs-12 mb-4">
                  <ol class="pl-2">
                    <li class="mb-2">折扣後的訂單小計金額 滿千免運，未達滿千常溫運費150元/冷凍運費200元</li>
                    <li class="mb-2">「營業用詢價」或「大量採購」請至 聯絡我們 留言，將有專人為您服務</li>
                  </ol>
                </div>
              </div>

              <div class="text-center mb-5">
                <div class="btn-box-1">
                  <a href="index.php" title="" class="button-style back">繼續購物</a>
                  <a href="checkout-inquery.php" title="下一步" class="button-style brown2">下一步</a>
                </div>
              </div>
            </div>
          </div>
          <!-- 冷凍宅配: END -->

          <!-- 常溫: START -->
          <div class="tab-pane fade" id="normal">
            <table class="table product-table table-bordered">
              <thead>
                <tr class="active">
                  <td class="text-nowrap">品名</td>
                  <!-- <td class="text-nowrap">商品規格</td> -->
                  <td class="">售價</td>
                  <td class="text-nowrap">數量</td>
                  <td class="text-nowrap">小計</td>
                  <td class="text-nowrap">刪除</td>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td class="text-center product-img">
                    <a href="#">
                      <img src="assets/images/p1-min.png">
                      <br>
                      常溫商品名稱商品名稱商品名稱商品名稱1
                    </a>
                  </td>
                  <!-- <td class="text-left">
                    <span class="mobile-th">商品規格</span>
                    <a href="#">test1</a>
                  </td> -->
                  <td class="text-right ">
                    <span class="mobile-th">售價</span>
                    NT $1,000
                  </td>
                  <td class="text-right">
                    <span class="mobile-th">數量</span>

                    <div class="input-number" data-max="10" data-min="1" data-step="1">
                      <a href="javascript:void(0)" class="minus">-</a>
                      <input type="text" value="1">
                      <a href="javascript:void(0)" class="plus">+</a>
                    </div>
                  </td>
                  <td class="text-right">
                    <span class="mobile-th">小計</span>
                    NT $1,000
                  </td>
                  <td class="text-right">
                    <span class="mobile-th">刪除</span>
                    <a class="btn btn-icon cart-del-btn"><i class="fa fa-trash"></i></a>
                  </td>
                </tr>
                <tr>
                  <td class="text-center product-img">
                    <a href="#">
                      <img src="assets/images/p1-min.png">
                      <br>
                      常溫商品名稱商品名稱商品名稱商品名稱1
                    </a>
                  </td>
                  <!-- <td class="text-left">
                    <span class="mobile-th">商品規格</span>
                    <a href="#">test1</a>
                  </td> -->
                  <td class="text-right ">
                    <span class="mobile-th">售價</span>
                    NT $1,000
                  </td>
                  <td class="text-right">
                    <span class="mobile-th">數量</span>

                    <div class="input-number" data-max="10" data-min="1" data-step="1">
                      <a href="javascript:void(0)" class="minus">-</a>
                      <input type="text" value="1">
                      <a href="javascript:void(0)" class="plus">+</a>
                    </div>
                  </td>
                  <td class="text-right">
                    <span class="mobile-th">小計</span>
                    NT $1,000
                  </td>
                  <td class="text-right">
                    <span class="mobile-th">刪除</span>
                    <a class="btn btn-icon cart-del-btn"><i class="fa fa-trash"></i></a>
                  </td>
                </tr>
              </tbody>
              <tfoot class="tfoot-borderless">
                <tr>
                  <td colspan="5" class="py-3 text-nowrap cart-total-td">
                    <div class="text-right pr-sm-3">
                      <h5 class="p-subject text-second my-0 py-0 align-middle d-inline-block">優惠券</h5>
                      <input type="text" class="form-control input-coupon" placeholder="請輸入優惠券代碼">
                      <button class="btn btn-main btn-coupon d-inline-block">使用</button>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td colspan="3" class="cart-total-empty-td"></td>
                  <th class="py-1 text-nowrap cart-total-th"><b>商品小計</b></th>
                  <td class="py-1 text-nowrap cart-total-td">NT $1,000</td>
                </tr>
                <tr>
                  <td colspan="3" class="cart-total-empty-td"></td>
                  <th class="py-1 text-nowrap cart-total-th"><b>折扣活動</b></th>
                  <td class="py-1 text-nowrap cart-total-td">NT $2,00</td>
                </tr>
                <tr>
                  <td colspan="3" class="cart-total-empty-td"></td>
                  <!-- 運費金額由後端帶入(分為冷凍宅配兩種) -->
                  <th class="py-1 text-nowrap cart-total-th"><b>運費</b></th>
                  <td class="py-1 text-nowrap cart-total-td">NT $2,00</td>
                </tr>
                <tr>
                  <td colspan="3" class="cart-total-empty-td"></td>
                  <th class="py-1 text-nowrap cart-total-th"><b>訂單總計</b></th>
                  <td class="py-1 text-nowrap cart-total-td">NT $1,200</td>
                </tr>
              </tfoot>
            </table>

            <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
              <div class="row pt-4">
                <div class="col-xs-12 mb-4">
                  <ol class="pl-2">
                    <li class="mb-2">折扣後的訂單小計金額 滿千免運，未達滿千常溫運費150元/冷凍運費200元</li>
                    <li class="mb-2">「營業用詢價」或「大量採購」請至 聯絡我們 留言，將有專人為您服務</li>
                  </ol>
                </div>
              </div>

              <div class="text-center mb-5">
                <div class="btn-box-1">
                  <a href="index.php" title="" class="button-style back">繼續購物</a>
                  <a href="checkout-inquery.php" title="下一步" class="button-style brown2">下一步</a>
                </div>
              </div>
            </div>

          </div>
          <!-- 冷凍宅配: END -->
          <!-- 常溫: END -->
        </div>
      </div>

    </div>
  </div>
</div>
<?php include('footer.php'); ?>

<script src="assets/js/wan-spinner.js"></script>
<script>
  $('.input-number').each(function(i,element) {
    var elem = $(element);
    var min = elem.data('min');
    var max = elem.data('max');
    var step = elem.data('step');
    var start = elem.find('input').val();
    elem.WanSpinner({
      start: start,
      step: step,
      minValue: min,
      maxValue: max,
      valueChanged: function() {
        return false;
      }
    });
  });
</script>