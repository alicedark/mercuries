
	<?php include('header.php'); ?>
	<div data-aos="fade-in" class="page-account" style="min-height: calc(100vh - 400px);">
    <nav class="breadcrumbwrap">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.php">首頁</a></li>
                <li class="breadcrumb-item active"><a href="">找不到頁面</a></li>
            </ol>
        </div>
    </nav>
    <h1 class="title-page">Error</h1>

    <div class="container text-center mt-5" style="margin-bottom: 40px;">
        <p>
          Error Page!
        </p>

        <div class="text-center pt-4">
          <div class="btn-box-1">
            <!-- <button class="button-style brown2">註冊</button> -->
            <a href="index.php" title="回首頁" class="button-style brown2">回首頁</a>
          </div>
        </div>
    </div>
</div>
<?php include('footer.php'); ?>