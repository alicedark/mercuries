<?php include('header.php'); ?>
<div class="page-account" data-aos="fade-in">
  <nav class="breadcrumbwrap">
      <div class="container">
          <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="index.php">首頁</a></li>
              <li class="breadcrumb-item"><a href="">會員中心</a></li>
              <li class="breadcrumb-item active"><a href="">會員資料變更</a></li>
          </ol>
      </div>
  </nav>
	<h1 class="title-page">會員資料變更</h1>
  
  <div class="container px-5 pb-lg-5 pb-4">
    <div class="row">
      <div class="col-12">
        <div>
          <form action="success.php">
            <div class="row">
              <div class="col-lg-6">
                <div class="row">
                  <div class="col-xs-12">
                    <div class="form-group mb-4">
                      <label>帳號(E-mail)</label>
                      <!-- <div class="">email@gmail.com</div> -->
                      <input class="form-control" type="email" disabled />
                    </div>
                  </div>

                  <div class="col-sm-6">
                    <div class="form-group mb-4">
                      <label>姓名</label>
                      <input class="form-control" type="text" />
                    </div>
                  </div>

                  <div class="col-sm-6">
                    <div class="form-group mb-4">
                      <label>行動電話</label>
                      <input class="form-control" type="text" />
                    </div>
                  </div>

                  <div class="col-sm-6">
                    <div class="form-group mb-4">
                      <label>室內電話</label>
                      <input class="form-control" type="text" />
                    </div>
                  </div>

                  <div class="col-sm-6">
                    <div class="form-group mb-4">
                      <label>生日</label>
                      <input class="form-control py-0" type="date" />
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-lg-6">
                <?php include('_address_combo.php'); ?>
                <div class="row mb-5">
                    <div class="col-sm-6">
                        <div class="form-group mb-4">
                            <label>是否為餐飲店家?</label>
                            <br>
                            <div class="radio-inline">
                            <label>
                                <input type="radio" name="is_business" id="optionsRadios1" value="m" checked>
                                是
                            </label>
                            </div>

                            <div class="radio-inline">
                            <label>
                                <input type="radio" name="is_business" id="optionsRadios2" value="f">
                                否
                            </label>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group mb-4">
                            <label>電子報訂閱</label>
                            <br>
                            <div class="radio-inline">
                            <label>
                                <input type="radio" name="edm" id="optionsRadios1" value="y" checked>
                                訂閱
                            </label>
                            </div>

                            <div class="radio-inline">
                            <label>
                                <input type="radio" name="edm" id="optionsRadios2" value="n">
                                取消訂閱
                            </label>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
            </div>

            <div class="text-center">
              <div class="btn-box-1">
                <a href="account.php" title="返回" class="button-style back mr-3">返回</a>
                <!-- <button class="button-style brown2">確認</button> -->
                <a href="account.php" title="確認" class="button-style brown2">確認</a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<?php include('footer.php'); ?>