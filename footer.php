<!-- Page Footer Start -->
<div class="fixed-box">
		<a href="cart.php" title="購物車" class="cart">
			<span class="num">99</span>
			<i class="fas fa-shopping-cart"></i>
		</a>
		<a href="cart-inquery.php" title="詢問單" class="inquiry">
			<span class="num">2</span>
			<span class="icon"></span>
		</a>
	</div>
	<footer id="footer">
		<div class="top-area">
			<div class="p-1200">
				<div class="logo"><img src="assets/images/logo-2-min.png" alt=""></div>
				<div class="info-wrap">
					<div class="left-area">
						<div class="item">
							<div class="title">服務電話</div>
							<div class="text"><a href="tel:+886-2-2299-2733">0800-004-433</a></div>
						</div>
						<div class="item">
							<div class="title">總公司傳真</div>
							<div class="text">02-2299-2733</div>
						</div>
						<div class="item no-title">
							<div class="text"><a href="https://www.google.com.tw/maps/place/248%E6%96%B0%E5%8C%97%E5%B8%82%E4%BA%94%E8%82%A1%E5%8D%80%E4%BA%94%E6%AC%8A%E8%B7%AF57%E8%99%9F">248新北市五股區五權路57號</a></div>
						</div>
					</div>
					<div class="right-area">
						<a href="https://www.facebook.com/pg/mlf0800004433/" title="" class="item">
							<span class="icon fb"><i class="fab fa-facebook-square"></i></span>
							<span class="title">三商食品粉絲專頁</span>
						</a>
						<a href="" title="" class="item">
							<span class="icon line"><i class="fab fa-line"></i></span>
							<span class="title">加入三商食品好友</span>
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="menu-area">
			<div class="p-1200">
				<ul class="reset">
					<li><a href="contact.php" title="聯絡我們">聯絡我們</a></li>
					<li><a href="recruit.php" title="人才招募">人才招募</a></li>
					<li><a href="notes.php" title="購物須知">購物須知</a></li>
					<li><a href="info.php" title="隱私權政策">隱私權政策</a></li>
				</ul>
			</div>
		</div>
		<div class="copy-area">
			<div class="p-1200">
				Copyright &copy; yyyy 網站名稱 All rights reserved. Codepulse-網站架設
			</div>
		</div>
		<div class="down-area">
			<div class="p-1200">
				<span>禁止酒駕</span><span class="icon"></span><span>飲酒過量<span class="xs-hide">，</span>有害健康</span>
			</div>
		</div>
	</footer>
	<!-- Page Footer End -->
</div>

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> -->
<!-- Core -->
<!--[if lt IE 9]>
<script src="assets/js/plugins/jQuery/jquery-1.12.4.min.js"></script>
<![endif]-->
<!--[if gte IE 9]><!-->
<script src="assets/js/plugins/jQuery/jquery-2.2.4.min.js"></script>
<!--<![endif]-->
<script src="assets/js/plugins/jQuery/jquery.mobile-1.4.5.custom.min.js"></script>
<script src="assets/js/plugins/Bootstrap/bootstrap-3.3.7.min.js"></script>
<!-- Plugins -->
<script src="assets/js/aos.js"></script>
<script src="assets/js/plugins/jQuery/jquery.slick-1.9.0.min.js"></script>
<!-- Scripts -->
<script src="assets/js/common.js?t=<?=time()?>"></script>


</body>
</html>