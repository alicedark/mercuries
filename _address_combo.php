<div class="row">
    <div class="col-sm-4">
        <div class="item form-group mb-4">
            <label>縣市 <span class="text-danger">*</span></label>
            <select class="form-control">
                <option>新北市</option>
                <option>台北市</option>
                <option>基隆市</option>
            </select>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="item form-group mb-4">
            <label>地區 <span class="text-danger">*</span></label>
            <select class="form-control">
                <option>板橋區</option>
                <option>土城區</option>
                <option>中和區</option>
            </select>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="item form-group mb-4">
            <label>郵遞區號</label>
            <input type="text" class="form-control read-only" readonly>
        </div>
    </div>
</div>
<div class="item form-group mb-4">
    <label>地址 <span class="text-danger">*</span></label>
    <input type="text" class="form-control" required>
</div>