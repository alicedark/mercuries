<?php include('header.php'); ?>
<div data-aos="fade-in" class="page-shopping">
    <nav class="breadcrumbwrap">
        <div class="p-1200">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="index.php">首頁</a></li>
				<li class="breadcrumb-item"><a href="">購物商城</a></li>
				<li class="breadcrumb-item"><a href="">第一層</a></li>
				<li class="breadcrumb-item"><a href="">酒類</a></li>
				<li class="breadcrumb-item"><a href="">大分類A</a></li>
				<li class="breadcrumb-item active"><a href="#">品牌</a></li>
			</ol>
        </div>
    </nav>
	<div id="product-list">
        <div class="p-1200">
            <div class="s-bn mb-4" style="background-image:url(assets/images/shopping-index-bn-min.png)">
            </div>
			<div class="p-content">
				<aside id="aside">
					<?php include('aside.php'); ?>
				</aside>
                <div class="p-main">
                    <div class="product-card-list">
						<div class="item">
							<div class="box">
								<div class="img"><img src="assets/images/product_3.jpg" alt="*" class="rwd-img"></div>
								<div class="main">
									<a href="product_view.php" class="title">SOUR3沙瓦 芭樂柳橙風味 350ml（24入）</a>
									<div class="price">
										<div class="new">NT$1,050</div>
										<div class="old">NT$1,280</div>
									</div>
									<a href="cart.php" class="more">
										<button name="" type="" class="button-style brown">加入購物車</button>
                                    </a>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="box">
								<div class="img"><img src="assets/images/product_1.jpg" alt="*" class="rwd-img"></div>
								<div class="main">
									<a href="product_view.php" class="title">SOUR3沙瓦 芭樂柳橙風味 350ml（24入）</a>
									<div class="price">
										<div class="new">NT$1,050</div>
										<div class="old">NT$1,280</div>
									</div>
									<a href="cart.php" class="more">
										<button name="" type="" class="button-style brown">加入個人詢問單</button>
                                    </a>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="box">
								<div class="img"><img src="assets/images/product_2.jpg" alt="*" class="rwd-img"></div>
								<div class="main">
									<a href="product_view.php" class="title">SOUR3沙瓦 芭樂柳橙風味 350ml（24入）</a>
									<div class="price">
										<div class="new">NT$1,050</div>
										<div class="old">NT$1,280</div>
									</div>
									<a href="cart.php" class="more">
										<button name="" type="" class="button-style brown">加入個人詢問單</button>
                                    </a>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="box">
								<div class="img"><img src="assets/images/product_3.jpg" alt="*" class="rwd-img"></div>
								<div class="main">
									<a href="product_view.php" class="title">SOUR3沙瓦 芭樂柳橙風味 350ml（24入）</a>
									<div class="price">
										<div class="new">NT$1,050</div>
										<div class="old">NT$1,280</div>
									</div>
									<a href="cart.php" class="more">
										<button name="" type="" class="button-style brown">加入購物車</button>
                                    </a>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="box">
								<div class="img"><img src="assets/images/product_1.jpg" alt="*" class="rwd-img"></div>
								<div class="main">
									<a href="product_view.php" class="title">SOUR3沙瓦 芭樂柳橙風味 350ml（24入）</a>
									<div class="price">
										<div class="new">NT$1,050</div>
										<div class="old">NT$1,280</div>
									</div>
									<a href="cart.php" class="more">
										<button name="" type="" class="button-style brown">加入購物車</button>
                                    </a>
								</div>
							</div>
						</div>
						<div class="item"></div>
						<div class="item"></div>
                    </div>
                    <?php include('page_paginate.php'); ?>
                </div>
			</div>
        </div>
	</div>
</div>
<?php include('footer.php'); ?>
<script>
    $('#aside .menu-list li.has_menu>a').data('switch', 'open').on('click', function(e) {
    e.preventDefault();
    var $ele = $(this), $eleLi = $ele.parent('li');
    if ($ele.data('switch') == 'open') {
      $ele.data('switch', 'close');
      $eleLi.addClass('open');
    } else {
      $ele.data('switch', 'open');
      $eleLi.removeClass('open');
    }
  });
</script>