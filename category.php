
	<?php include('header.php'); ?>
	<div data-aos="fade-in" class="page-category">
        <div class="bn">
            <img src="assets/images/product-bn.jpg" alt="" class="rwd-img">
        </div>
        <nav class="breadcrumbwrap">
            <div class="container">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.php">首頁</a></li>
                    <li class="breadcrumb-item active"><a href="">活動新訊</a></li>
                </ol>
            </div>
        </nav>
        <h1 class="title-page">活動新訊</h1>
        <div class="container" style="margin-bottom: 40px;">
            <ul class="nav nav-pills text-center mb-3">
                <li class="active"><a data-toggle="tab" href="">分類名稱</a></li>
                <li><a data-toggle="tab" href="">分類名稱</a></li>
                <li><a data-toggle="tab" href="">分類名稱</a></li>
            </ul>
            <div class="news-card-list mt-4">
                <a href="category2.php" class="news-card">
                    <div class="image-box">
                        <div class="image" style="background-image:url(assets/images/section-1-1-min.png)"></div>
                    </div>
                    <div class="text-box">
                        <h1>即時活動 熱銷商品免額滿運</h1>
                        <span class="date mb-2"><i class="fa fa-calendar mr-1"></i>即日起~2020/9/15</span>
                        <span>購買 「珈琲鑑定士 經典微糖(30入/箱)」 一箱，即隨貨贈送同商品一箱；購買2箱送2箱，以此類推。</span>
                    </div>
                </a>
                <a href="category2.php" class="news-card">
                    <div class="image-box">
                        <div class="image" style="background-image:url(assets/images/section-1-2-min.png)"></div>
                    </div>
                    <div class="text-box">
                        <h1>即時活動 熱銷商品免額滿運</h1>
                        <span class="date mb-2"><i class="fa fa-calendar mr-1"></i>即日起~2020/9/15</span>
                        <span>購買 「珈琲鑑定士 經典微糖(30入/箱)」 一箱，即隨貨贈送同商品一箱；購買2箱送2箱，以此類推。</span>
                    </div>
                </a>
                <a href="category2.php" class="news-card">
                    <div class="image-box">
                        <div class="image" style="background-image:url(assets/images/section-1-3-min.png)"></div>
                    </div>
                    <div class="text-box">
                        <h1>即時活動 熱銷商品免額滿運</h1>
                        <span class="date mb-2"><i class="fa fa-calendar mr-1"></i>即日起~2020/9/15</span>
                        <span>購買 「珈琲鑑定士 經典微糖(30入/箱)」 一箱，即隨貨贈送同商品一箱；購買2箱送2箱，以此類推。</span>
                    </div>
                </a>
                <a href="category2.php" class="news-card">
                    <div class="image-box">
                        <div class="image" style="background-image:url(assets/images/section-1-2-min.png)"></div>
                    </div>
                    <div class="text-box">
                        <h1>即時活動 熱銷商品免額滿運</h1>
                        <span class="date mb-2"><i class="fa fa-calendar mr-1"></i>即日起~2020/9/15</span>
                        <span>購買 「珈琲鑑定士 經典微糖(30入/箱)」 一箱，即隨貨贈送同商品一箱；購買2箱送2箱，以此類推。</span>
                    </div>
                </a>
                <a href="category2.php" class="news-card">
                    <div class="image-box">
                        <div class="image" style="background-image:url(assets/images/section-1-1-min.png)"></div>
                    </div>
                    <div class="text-box">
                        <h1>即時活動 熱銷商品免額滿運</h1>
                        <span class="date mb-2"><i class="fa fa-calendar mr-1"></i>即日起~2020/9/15</span>
                        <span>購買 「珈琲鑑定士 經典微糖(30入/箱)」 一箱，即隨貨贈送同商品一箱；購買2箱送2箱，以此類推。</span>
                    </div>
                </a>
                <a href="category2.php" class="news-card">
                    <div class="image-box">
                        <div class="image" style="background-image:url(assets/images/section-1-3-min.png)"></div>
                    </div>
                    <div class="text-box">
                        <h1>即時活動 熱銷商品免額滿運</h1>
                        <span class="date mb-2"><i class="fa fa-calendar mr-1"></i>即日起~2020/9/15</span>
                        <span>購買 「珈琲鑑定士 經典微糖(30入/箱)」 一箱，即隨貨贈送同商品一箱；購買2箱送2箱，以此類推。</span>
                    </div>
                </a>
                <a href="category2.php" class="news-card">
                    <div class="image-box">
                        <div class="image" style="background-image:url(assets/images/section-1-3-min.png)"></div>
                    </div>
                    <div class="text-box">
                        <h1>即時活動 熱銷商品免額滿運</h1>
                        <span class="date mb-2"><i class="fa fa-calendar mr-1"></i>即日起~2020/9/15</span>
                        <span>購買 「珈琲鑑定士 經典微糖(30入/箱)」 一箱，即隨貨贈送同商品一箱；購買2箱送2箱，以此類推。</span>
                    </div>
                </a>
                <a href="#" class="news-card" style="border:0">
                </a>
                <a href="#" class="news-card" style="border:0">
                </a>


            </div>
            <?php include('page_paginate.php'); ?>
        </div>
    </div>

<!-- my footer start -->

<?php include('footer.php'); ?>