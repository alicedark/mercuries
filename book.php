<!DOCTYPE html>
<html lang="en" class="no-js demo-4">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
		<title>電子書</title>
        <meta name="author" content="Codrops" />
		<link href="assets/css/bootstrap-3.3.7.min.css" rel="stylesheet">

		<link rel="stylesheet" type="text/css" href="assets/css/default.css" />
		<link rel="stylesheet" type="text/css" href="assets/css/bookblock.css" />
		<!-- custom demo style -->
		<link rel="stylesheet" type="text/css" href="assets/css/book.css" />
		<link href="assets/css/fontawesome-5.4.1-all.min.css" rel="stylesheet">
        <script src="assets/js/modernizr.custom.js"></script>
        <style>
            img{
                max-width: 100%;
                width: 100%;
            }
            @media (max-width: 767px){
                .bb-custom-wrapper .bb-bookblock{
                    height:70vw;
                }
            }
            @media (min-width: 768px){
                .container {
                    width: 750px;
                }
            }
            @media (min-width: 992px){
                .container {
                    width: 970px;
                }
            }
            @media (min-width: 1200px){
                .container {
                    width: 1170px;
                }
            }

            .bb-custom-icon-arrow-left:before, .bb-custom-icon-arrow-right:before {
                font-family : 'Font Awesome 5 Free';
                font-weight : 900;
                content : '\f105';
			}
			.bb-custom-icon-first:before, .bb-custom-icon-last:before{
                font-family : 'Font Awesome 5 Free';
                font-weight : 900;
                content : '\f101';
			}
            .bb-item {
                /* height : unset; */
            }

        </style>
	</head>
	<body>
		<div class="container" style="padding: 80px 0 0;width:768px;">
			<div class="bb-custom-wrapper">
				<div id="bb-bookblock" class="bb-bookblock" style="height:546px;">
					<div class="bb-item">
						<div class="bb-custom-firstpage">
						</div>
						<div class="bb-custom-side">
                            <a href=""><img src="assets/images/book01-1.jpg" alt=""></a>
						</div>
					</div>
					<div class="bb-item">
						<div class="bb-custom-side">
                            <a href=""><img src="assets/images/book01-1.jpg" alt=""></a>
						</div>
						<div class="bb-custom-side">
                            <a href=""><img src="assets/images/book01-2.jpg" alt=""></a>
						</div>
					</div>
				</div>

				<nav>
					<a id="bb-nav-first" href="#" class="bb-custom-icon bb-custom-icon-first">First page</a>
					<a id="bb-nav-prev" href="#" class="bb-custom-icon bb-custom-icon-arrow-left">Previous</a>
					<a id="bb-nav-next" href="#" class="bb-custom-icon bb-custom-icon-arrow-right">Next</a>
					<a id="bb-nav-last" href="#" class="bb-custom-icon bb-custom-icon-last">Last page</a>
				</nav>

			</div>
        </div>
        
        
        <a href="magazine.php" style="display: block;
    width: 150px;
    margin: 30px auto 80px;
    padding: 10px;
    border-radius: 40px;
    background: #be8915;
    color: #fff;
    text-align: center;">回到三商</a>
        
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
		<script src="assets/js/jquerypp.custom.js"></script>
		<script src="assets/js/jquery.bookblock.js"></script>
		<script>
			var Page = (function() {
				
				var config = {
						$bookBlock : $( '#bb-bookblock' ),
						$navNext : $( '#bb-nav-next' ),
						$navPrev : $( '#bb-nav-prev' ),
						$navFirst : $( '#bb-nav-first' ),
						$navLast : $( '#bb-nav-last' )
					},
					init = function() {
						config.$bookBlock.bookblock( {
							speed : 1000,
							shadowSides : 0.8,
							shadowFlip : 0.4
						} );
						initEvents();
					},
					initEvents = function() {
						
						var $slides = config.$bookBlock.children();

						// add navigation events
						config.$navNext.on( 'click touchstart', function() {
							config.$bookBlock.bookblock( 'next' );
							return false;
						} );

						config.$navPrev.on( 'click touchstart', function() {
							config.$bookBlock.bookblock( 'prev' );
							return false;
						} );

						config.$navFirst.on( 'click touchstart', function() {
							config.$bookBlock.bookblock( 'first' );
							return false;
						} );

						config.$navLast.on( 'click touchstart', function() {
							config.$bookBlock.bookblock( 'last' );
							return false;
						} );
						
						// add swipe events
						$slides.on( {
							'swipeleft' : function( event ) {
								config.$bookBlock.bookblock( 'next' );
								return false;
							},
							'swiperight' : function( event ) {
								config.$bookBlock.bookblock( 'prev' );
								return false;
							}
						} );

						// add keyboard events
						$( document ).keydown( function(e) {
							var keyCode = e.keyCode || e.which,
								arrow = {
									left : 37,
									up : 38,
									right : 39,
									down : 40
								};

							switch (keyCode) {
								case arrow.left:
									config.$bookBlock.bookblock( 'prev' );
									break;
								case arrow.right:
									config.$bookBlock.bookblock( 'next' );
									break;
							}
						} );
					};

					return { init : init };

			})();
		</script>
		<script>
				Page.init();
		</script>
	</body>
</html>