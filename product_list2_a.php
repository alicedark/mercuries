<?php include('header.php'); ?>
<div data-aos="fade-in" class="page-shopping">
	<nav class="breadcrumbwrap">
		<div class="p-1200">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.php">首頁</a></li>
                <li class="breadcrumb-item"><a href="">購物商城</a></li>
                <li class="breadcrumb-item active"><a href="#">第一層</a></li>
            </ol>
		</div>
    </nav>
    
	<div id="product-list">
        <div class="p-1200">
            <div class="s-bn mb-4" style="background-image:url(assets/images/shopping-index-bn-min.png)">
                <div class="text-box">
                    <!-- <h1>購物商城</h1> -->
                </div>
            </div>
            <div class="product-card-list mt-2">
                <a href="product_list3_a.php" class="product-card for-4">
                    <div class="bg" style="background-image:url(assets/images/section-1-1-min.png)"></div>
                    <div class="text-box">
                        <h1>酒類</h1>
                    </div>
                </a>
                <a href="product_list3_a.php" class="product-card for-4">
                    <div class="bg" style="background-image:url(assets/images/section-1-1-min.png)"></div>
                    <div class="text-box">
                        <h1>食品調味料</h1>
                    </div>
                </a>
                <a href="product_list3_a.php" class="product-card for-4">
                    <div class="bg" style="background-image:url(assets/images/section-1-1-min.png)"></div>
                    <div class="text-box">
                        <h1>國內產品</h1>
                    </div>
                </a>
                <a href="product_list3_a.php" class="product-card for-4">
                    <div class="bg" style="background-image:url(assets/images/section-1-1-min.png)"></div>
                    <div class="text-box">
                        <h1>產品分類名稱</h1>
                    </div>
                </a>
                <a href="product_list3_a.php" class="product-card for-4">
                    <div class="bg" style="background-image:url(assets/images/section-1-1-min.png)"></div>
                    <div class="text-box">
                        <h1>產品分類名稱</h1>
                    </div>
                </a>
                <a href="product_list3_a.php" class="product-card for-4">
                    <div class="bg" style="background-image:url(assets/images/section-1-1-min.png)"></div>
                    <div class="text-box">
                        <h1>產品分類名稱</h1>
                    </div>
                </a>
                <a class="for-4">
                </a>
                <a class="for-4">
                </a>
            </div>
        </div>
	</div>
</div>
<?php include('footer.php'); ?>