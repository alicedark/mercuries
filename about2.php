
	<?php include('header.php'); ?>
	<div data-aos="fade-in" class="page-info bg-gray">

        <nav class="breadcrumbwrap">
            <div class="container">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.php">首頁</a></li>
                    <li class="breadcrumb-item active"><a href="#">飲食店樣向</a></li>
                </ol>
            </div>
        </nav>
        <h1 class="title-page">飲食店様向け</h1>
        <div class="container">
            <div class="sec-block">
                <div class="row">
                    <div class="col-sm-6 mb-4">
                        <h1 class="subtitle-page">会社概要</h1>
                        <table class="table1">
                            <tr>
                                <th>代表者</th>
                                <td>董事長 陳翔玢</td>
                            </tr>
                            <tr>
                                <th>資本金</th>
                                <td>NT$200,000,000</td>
                            </tr>
                            <tr>
                                <th>從業員數</th>
                                <td>130位</td>
                            </tr>
                            <tr>
                                <th>本社所在地</th>
                                <td>台灣新北市五股區五權路57號</td>
                            </tr>
                            <tr>
                                <th>本社電話</th>
                                <td>(02) 2299-2833</td>
                            </tr>
                            <tr>
                                <th>設立</th>
                                <td>2008年10月</td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-sm-6">
                        <h1 class="subtitle-page">会社創立</h1>
                        <p>三商食品は1990年に設立した三商行食品事業部としてスタートしました。台湾における酒類、飲料、調味料の輸入販売会社であります。</p>
                        <p>酒類の「いいちこ焼酎」、「霧島焼酎」、「白岳焼酎」、「黄桜清酒」、「小山本家清酒」、「松竹梅清酒」等。調味料の「Kewpieドレッシング」、「Bulldogソース」、「金印わさび」、「日の出みりん」、「マルキン醤油」等。幅広い取扱い商品によりお客様にアプローチ。</p>
                        <p>台北、桃園、新竹、台中、高雄に営業拠点、自社倉庫と自社トラックを持ち、飲食店や小売チャネルに直販を行います。お客様が必要とする商品を一括して承って届けます。</p>
                    </div>
                    <div class="col-lg-12 mt-5 mb-4">
                        <h1 class="subtitle-page">強み</h1>
                        <h3>単一窓口の全国配送</h3>
                        <div class="sub-text">
                            <ul>
                                <li>専用フリーダイヤル、専用発注アプリでいつでもどこでも発注できます。</li>
                                <li>一日3便、周6日の配送、全国どこでも届けします。</li>
                            </ul>
                        </div>
                        <img src="assets/images/about_2-1.png" class="rwd-img" alt="">
                    </div>
                    <div class="col-sm-9 mt-5">
                        <h3>プロフェッショナルがあらゆる面から飲食店経営をサポート</h3>
                        <p class="sub-text">営業スタッフは単なる御用聞きではありません。トレンドのお酒の情報やメニュー提案も行います。単なる商品の提案に留まらず、エンドユーザーに喜んで頂く商材の提案を心がけています。例えば、樽生ビールの品質管理、サワーレシピー、キャンペーン、販促POP等を提案します。</p>
                    </div>
                    <div class="col-sm-3 mt-5">
                        <img src="assets/images/about_2-2.png" class="rwd-img" alt="">
                    </div>
                    <div class="col-sm-12 mt-5">
                        <h3>直販体制</h3>
                        <p class="sub-text">より良いサービスを提供するため、卸し業者を通さずに、台湾における9割の日本料理店とその他の飲食店約5,000店舗と直接取引しています。</p>
                        <img src="assets/images/about_2-3.png" class="rwd-img" alt="" style="max-width: 900px;">
                    </div>
                    <div class="col-sm-7 mt-5">
                        <h3>強大な営業管理</h3>
                        <p class="sub-text">全国に五つの営業拠点に直販体制の営業チームがスピティーにお客様のニーズに対応します。独自のERPシステムで1件1件の受注に効率よく配車できます。完備した内勤システムで営業管理をサポートします。</p>
                    </div>
                    <div class="col-sm-5 mt-5">
                        <img src="assets/images/about_2-4.png" class="rwd-img" alt="">
                    </div>
                    <div class="col-sm-6 mt-5">
                        <h3>商品価格の優勢</h3>
                        <p class="sub-text">自社直輸入して直販のため、コストパフォーマスが高い商品をお客様に提供します。</p>
                        <img src="assets/images/about_2-5.png" class="rwd-img" alt="" style="max-width: 350px;">
                    </div>
                    <div class="col-sm-6 mt-5">
                        <h3>三商行グループに属する</h3>
                        <p class="sub-text">台湾の流通市場は近年大きく変化の中、三商食品は株上場の三商グループに属し、健全な経営で成長し続けます。</p>
                        <img src="assets/images/about_2-6.png" class="rwd-img" alt="" style="max-width: 350px;">
                    </div>
                    <div class="col-sm-12 mt-5">
                        <h3>日本語の問合せ</h3>
                        <p class="sub-text" style="line-height: 30px;">
                            E-mail: service@mlf.com.tw<br>
                            Phone: 02-2299-2833 #6011 / 6013 / 6014
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

<!-- my footer start -->

<?php include('footer.php'); ?>