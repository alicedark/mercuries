<?php include('header.php'); ?>
<div class="page-account" data-aos="fade-in">
  <nav class="breadcrumbwrap">
      <div class="container">
          <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="index.php">首頁</a></li>
              <li class="breadcrumb-item"><a href="">會員中心</a></li>
              <li class="breadcrumb-item active"><a href="">修改密碼</a></li>
          </ol>
      </div>
  </nav>
  <h1 class="title-page">修改密碼</h1>

  <div class="container px-5 pb-lg-5 pb-4">
    <div class="row">
      <div class="col-12">
        <div>
          <form action="success.php">
            <div class="row px-lg-5">
              <div class="col-sm-6 col-sm-push-3">
                <div class="form-group mb-4">
                  <label>原密碼</label>
                  <input class="form-control" type="password" placeholder="原密碼" />
                  <div class="text-danger">密碼錯誤</div>
                </div>
                <div class="form-group mb-4">
                  <label>新密碼</label>
                  <input class="form-control" type="password" placeholder="新密碼" />
                </div>
                <div class="form-group mb-4">
                  <label>確認密碼</label>
                  <input class="form-control" type="password" placeholder="確認密碼" />
                </div>
                <div class="text-center">
                  <div class="btn-box-1">
                    <a href="account.php" title="返回" class="button-style back mr-3">返回</a>
                    <a href="account.php" title="確認" class="button-style brown2">確認</a>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<?php include('footer.php'); ?>