<?php include('header.php'); ?>
<div class="page-account">
  <nav class="breadcrumbwrap">
      <div class="container">
          <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="index.php">首頁</a></li>
              <li class="breadcrumb-item active"><a href="">會員中心</a></li>
          </ol>
      </div>
  </nav>
  <h1 class="title-page">會員中心</h1>
  <div class="container pb-lg-5 pb-4 px-5">
    <!-- 導覽列 BreadCrumb -->
    <div class="row">
        <div class="col-lg-8 col-lg-push-2">
            <div class="mt-lg-3 mt-0">
                <div class="row no-gutters member-func">
                    <div class="col-xs-6 col-md-8"></div>

                    <div class="col-xs-6 col-md-4 text-center mb-5">
                        <a href="login.php">
                          <i class="fas fa-sign-out-alt"></i> 會員登出
                        </a>
                    </div>

                    <div class="col-xs-6 col-md-4">
                        <div class="card">
                            <a href="edit.php">
                              <i class="fas fa-id-card"></i>
                              <h4>修改會員資料</h4>
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-6 col-md-4">
                        <div class="card">
                            <a href="password.php">
                                <i class="fas fa-shield-alt"></i>
                                <h4>修改密碼</h4>
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-6 col-md-4">
                        <div class="card">
                            <a href="order-history.php">
                              <i class="fas fa-list-alt"></i>
                              <h4>詢問單記錄</h4>
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-6 col-md-4">
                        <div class="card">
                            <a href="order-history.php">
                              <i class="fas fa-shopping-bag"></i>
                              <h4>訂單記錄</h4>
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-6 col-md-4">
                        <div class="card">
                            <a href="addresslist.php">
                              <i class="fas fa-address-book"></i>
                              <h4>地址簿</h4>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>
<?php include('footer.php'); ?>