<?php include('header.php'); ?>
<div data-aos="fade-in" class="page-shopping">
    <nav class="breadcrumbwrap">
        <div class="p-1200">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.php">首頁</a></li>
                <li class="breadcrumb-item"><a href="">餐飲用產品</a></li>
                <li class="breadcrumb-item"><a href="">酒類</a></li>
                <li class="breadcrumb-item"><a href="">大分類A</a></li>
                <li class="breadcrumb-item active"><a href="#">品牌</a></li>
            </ol>
        </div>
    </nav>
    <h1 class="title-page">品牌</h1>

	<div id="product-list">
        <div class="p-1200">
			<div class="p-content">
				<aside id="aside">
					<?php include('aside.php'); ?>
				</aside>
                <div class="p-main">
					<div class="s-bn mb-4" style="background-image:url(assets/images/shopping-index-bn-min.png)">
						<div class="text-box has-info">
							<h1>品牌A</h1>
							<p>日本美乃滋創始品牌！<br>
								台灣唯一正式代理！<br>
								美乃滋可運用於沾醬、炸物、廣島燒、章魚燒醬；<br>
								混合明太子後作焗烤醬等，用途廣泛！</p>
						</div>
					</div>
					<div class="product-list2">
						<ul class="reset" data-plugins="product-list2">
							<div class="item">
								<a href="product_view2.php" title="">
									<span class="img"><img src="assets/images/p1-min.png" alt=""></span>
									<span class="main mb-2">
										<span class="title">丸金 特級淡口醬油 1000ml</span>
										
										
									</span>
								</a>
							</div>
							<div class="item">
								<a href="product_view2.php" title="">
									<span class="img"><img src="assets/images/p2-min.png" alt=""></span>
									<span class="main mb-2">
										<span class="title">即期良品促銷- 富澤商店 烘焙材料烘焙材料</span>
										
									</span>
								</a>
							</div>
							<div class="item">
								<a href="product_view2.php" title="">
									<span class="img"><img src="assets/images/p3-min.png" alt=""></span>
									<span class="main mb-2">
										<span class="title">丸金 特級淡口醬油 1000ml</span>
										
										
									</span>
								</a>
							</div>
							<div class="item">
								<a href="product_view2.php" title="">
									<span class="img"><img src="assets/images/p1-min.png" alt=""></span>
									<span class="main mb-2">
										<span class="title">丸金 特級淡口醬油 1000ml</span>
										
										
									</span>
								</a>
							</div>
							<div class="item">
								<a href="product_view2.php" title="">
									<span class="img"><img src="assets/images/p2-min.png" alt=""></span>
									<span class="main mb-2">
										<span class="title">即期良品促銷- 富澤商店 烘焙材料烘焙材料</span>
										
									</span>
								</a>
							</div>
							<div class="item">
								<a href="product_view2.php" title="">
									<span class="img"><img src="assets/images/p3-min.png" alt=""></span>
									<span class="main mb-2">
										<span class="title">丸金 特級淡口醬油 1000ml</span>
										
										
									</span>
								</a>
							</div>
						</ul>
					</div>
                    <?php include('page_paginate.php'); ?>
                </div>
			</div>
        </div>
	</div>
</div>
<?php include('footer.php'); ?>
<script>
    $('#aside .menu-list li.has_menu>a').data('switch', 'open').on('click', function(e) {
    e.preventDefault();
    var $ele = $(this), $eleLi = $ele.parent('li');
    if ($ele.data('switch') == 'open') {
      $ele.data('switch', 'close');
      $eleLi.addClass('open');
    } else {
      $ele.data('switch', 'open');
      $eleLi.removeClass('open');
    }
  });
</script>