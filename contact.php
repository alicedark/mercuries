
	<?php include('header.php'); ?>
	<div data-aos="fade-in">

        <nav class="breadcrumbwrap">
            <div class="container">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.php">首頁</a></li>
                    <li class="breadcrumb-item active"><a href="_login.php">聯絡我們</a></li>
                </ol>
            </div>
        </nav>
        <div class="container" style="margin-bottom: 40px;">
            <h1 class="title-page">聯絡我們</h1>
            <!-- <div class="desc-page">如果您對本公司有任何疑問、需求或建議，請依照我們所提供的表單欄位，詳細填寫基本資料及諮詢內容，我們將會有專人竭誠為您服務，非常感謝您的來信！！ 
                <br><b>【*為必填項目】</b></div> -->
            <div class="row">
                <div class="col-sm-12 text-center mb-5">
                    <p>還不是會員嗎?立即加入獲得更多會員專屬服務內容</p>
                    <div class="text-center">
                        <div class="btn-box-1">
                            <a href="login.php" title="確認送出" class="button-style brown2">登入會員</a>
                            <a href="register.php" title="確認送出" class="button-style">加入會員</a>
                        </div>
                    </div>
                </div>
                <form>
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="inputName">姓名*</label>
                                    <input type="text" class="form-control" id="inputName" placeholder="姓名">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="inputGender">性別</label><br>
                                    <div class="radio-inline">
                                        <label>
                                        <input type="radio" name="gender" id="optionsRadios1" value="0" checked>
                                        男
                                        </label>
                                    </div>

                                    <div class="radio-inline">
                                        <label>
                                        <input type="radio" name="gender" id="optionsRadios2" value="1">
                                        女
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="inputPhone">行動電話*</label>
                                    <input type="tel" class="form-control" id="inputPhone" placeholder="行動電話">
                                    <p class="text-danger">請確實填寫</p>
                                </div>
                                <div class="form-group">
                                    <label for="inputAddress">聯絡地址*</label>
                                    <input type="text" class="form-control" id="inputAddress" placeholder="聯絡地址">
                                </div>
                                <div class="form-group">
                                    <label for="imputEmail">電子信箱*</label>
                                    <input type="mail" class="form-control" id="imputEmail" placeholder="電子信箱">
                                </div>
                                <div class="form-group">
                                    <label>是否為餐飲店家？*</label><br>
                                    <div class="radio-inline">
                                        <label>
                                        <input type="radio" name="isRestaurant" id="optionsRadios1" value="0" checked>
                                        是
                                        </label>
                                    </div>

                                    <div class="radio-inline">
                                        <label>
                                        <input type="radio" name="isRestaurant" id="optionsRadios2" value="1">
                                        否
                                        </label>
                                    </div>
                                    <input type="text" class="form-control" id="restaurantInput" placeholder="敬請填寫店名，若為籌備中店家則略過">
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- 社群登入 -->
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="imputEmail">詢問我們*</label>
                            <div>
                                <div class="radio-btn-box">
                                    <input type="radio" name="contact-issue" id="contact-issue1" value="0" checked>
                                    <label class="radio-btn" for="contact-issue1">店家詢價</label>
                                </div>
                                <div class="radio-btn-box">
                                    <input type="radio" name="contact-issue" id="contact-issue2" value="1">
                                    <label class="radio-btn" for="contact-issue2">商品問題</label>
                                </div>
                                <div class="radio-btn-box">
                                    <input type="radio" name="contact-issue" id="contact-issue3" value="1">
                                    <label class="radio-btn" for="contact-issue3">異業合作</label>
                                </div>
                                <div class="radio-btn-box">
                                    <input type="radio" name="contact-issue" id="contact-issue4" value="1">
                                    <label class="radio-btn" for="contact-issue4">廠商提案</label>
                                </div>
                                <div class="radio-btn-box">
                                    <input type="radio" name="contact-issue" id="contact-issue5" value="1">
                                    <label class="radio-btn" for="contact-issue5">訂單問題</label>
                                </div>
                                <div class="radio-btn-box">
                                    <input type="radio" name="contact-issue" id="contact-issue6" value="1">
                                    <label class="radio-btn" for="contact-issue6">其他</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">留言*</label>
                            <textarea class="form-control" name="" id="contact-messenge" cols="30" rows="10" placeholder="敬請告知欲詢問的品項、類別或其他需求與建議，我們將會有專人竭誠為您服務，謝謝。"></textarea>
                        </div>
                        <div class="form-group">
                            <label>驗證碼<span class="text-danger">*</span></label>

                            <div class="clearfix">
                                <input type="text" name="captcha" id="input-captcha" class="form-control pull-left" style="width: calc(100% - 163px - 1rem);">
                                <img src="assets/images/capcha.jpg" alt="captcha" class="pull-right" style="height: calc(1.5em + 0.75rem - 2px);">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-sm-offset-6">
                    </div>
                    <div class="col-sm-12 text-center mt-4">
                        <div class="btn-box-1">
                            <button type="submit" class="button-style brown2">送出</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>

<!-- my footer start -->

<?php include('footer.php'); ?>