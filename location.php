<?php include('header.php'); ?>
	<div id="location">
		<div class="bn">
			<img src="assets/images/product-bn.jpg" alt="" class="rwd-img">
		</div>
        <nav class="breadcrumbwrap">
            <div class="container">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.php">首頁</a></li>
                    <li class="breadcrumb-item active"><a href="#">品嚐據點</a></li>
                </ol>
            </div>
        </nav>
        <h1 class="title-page">品嚐據點</h1>
		<!-- <div class="p-ban"><img src="assets/images/bn-2-min.png" alt=""></div> -->
		<div class="p-1200">
			<div class="select-wrap">
				<div class="title">門市檢索</div>
				<div class="group-main">
					<div class="group-select">
						<select name="">
							<option>請選擇縣市</option>
						</select>
					</div>
					<div class="group-select">
						<select name="">
							<option>全區</option>
						</select>
					</div>
					<div class="group-select last">
						<select name="">
							<option>全產品</option>
						</select>
					</div>
					<button name="" type="" class="btn-search"><i class="fas fa-search"></i></button>
				</div>
			</div>
			<div class="location-list">
				<!-- 01 -->
				<div class="item">
					<a href="#" title="" class="img"><img src="assets/images/logo-min.png" alt=""></a>
					<div class="main">
						<div class="title">高玉日本料理民生店</div>
						<div class="add">台北市中山區民生東路三段8號B1</div>
						<!-- 後臺提供連結欄位 -->
						<div class="links"><a href="#" title=""><i class="fas fa-link"></i></a></div>
					</div>
				</div>
				<!-- /01 -->
				<div class="item">
					<a href="#" title="" class="img"><img src="assets/images/logo-min.png" alt=""></a>
					<div class="main">
						<div class="title">高玉日本料理民生店</div>
						<div class="add">台北市中山區民生東路三段8號B1</div>
						<div class="links"><a href="#" title=""><i class="fas fa-link"></i></a></div>
					</div>
				</div>
				<div class="item">
					<a href="#" title="" class="img"><img src="assets/images/logo-min.png" alt=""></a>
					<div class="main">
						<div class="title">高玉日本料理民生店</div>
						<div class="add">台北市中山區民生東路三段8號B1</div>
						<div class="links"><a href="#" title=""><i class="fas fa-link"></i></a></div>
					</div>
				</div>
				<div class="item">
					<a href="#" title="" class="img"><img src="assets/images/logo-min.png" alt=""></a>
					<div class="main">
						<div class="title">高玉日本料理民生店</div>
						<div class="add">台北市中山區民生東路三段8號B1</div>
						<div class="links"><a href="#" title=""><i class="fas fa-link"></i></a></div>
					</div>
				</div>
				<div class="item">
					<a href="#" title="" class="img"><img src="assets/images/logo-min.png" alt=""></a>
					<div class="main">
						<div class="title">高玉日本料理民生店</div>
						<div class="add">台北市中山區民生東路三段8號B1</div>
						<div class="links"><a href="#" title=""><i class="fas fa-link"></i></a></div>
					</div>
				</div>
				<div class="item">
					<a href="#" title="" class="img"><img src="assets/images/logo-min.png" alt=""></a>
					<div class="main">
						<div class="title">高玉日本料理民生店</div>
						<div class="add">台北市中山區民生東路三段8號B1</div>
						<div class="links"><a href="#" title=""><i class="fas fa-link"></i></a></div>
					</div>
				</div>
				<div class="item">
					<a href="#" title="" class="img"><img src="assets/images/logo-min.png" alt=""></a>
					<div class="main">
						<div class="title">高玉日本料理民生店</div>
						<div class="add">台北市中山區民生東路三段8號B1</div>
						<div class="links"><a href="#" title=""><i class="fas fa-link"></i></a></div>
					</div>
				</div>
				<div class="item">
					<a href="#" title="" class="img"><img src="assets/images/logo-min.png" alt=""></a>
					<div class="main">
						<div class="title">高玉日本料理民生店</div>
						<div class="add">台北市中山區民生東路三段8號B1</div>
						<div class="links"><a href="#" title=""><i class="fas fa-link"></i></a></div>
					</div>
				</div>
				<div class="item">
					<a href="#" title="" class="img"><img src="assets/images/logo-min.png" alt=""></a>
					<div class="main">
						<div class="title">高玉日本料理民生店</div>
						<div class="add">台北市中山區民生東路三段8號B1</div>
						<div class="links"><a href="#" title=""><i class="fas fa-link"></i></a></div>
					</div>
				</div>
			</div>
			<?php include('page_paginate.php'); ?>
		</div>
	</div>
<?php include('footer.php'); ?>